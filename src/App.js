import React, { Fragment } from 'react'
import { Route, Switch } from 'react-router-dom'
import ReduxToastr from 'react-redux-toastr'
import LoadingBar from 'react-redux-loading-bar'

import routeOptions from './routes'

const App = () => {
  const routes = routeOptions.map(({ exact, component, path }, index) => (
    <Route key={index} exact={exact} component={component} path={path} />
  ))
  return (
    <Fragment>
      <LoadingBar
        style={{ backgroundColor: 'blue', height: '3px', zIndex: 9999 }}
      />
      <Switch>{routes}</Switch>
      <ReduxToastr
        timeOut={5000}
        newestOnTop={true}
        preventDuplicates={true}
        position="top-right"
        transitionIn="bounceIn"
        transitionOut="fadeOut"
        progressBar={false}
        closeOnToastrClick
      />
    </Fragment>
  )
}

export default App
