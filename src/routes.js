import DashboardPage from './components/Home'
import SetPasswordPage from './containers/Account/SetPassword'
import ResetPasswordPage from './containers/Account/ResetPassword'
import ForgotPasswordPage from './containers/Account/ForgotPassword'
import ChangePasswordPage from './containers/Account/ChangePassword'
import LoginPage from './containers/Account/Login'

import UserMgtPage from './containers/Users'
import RoleMgtPage from './containers/Roles'

import TenantPage from './containers/Tenancy/Tenants'
import ClientPage from './containers/Tenancy/Clients'

import CustTypePage from './containers/Configuration/CustomerType'
import SubCustTypePage from './containers/Configuration/SubCustomerType'
import BillingTypePage from './containers/Configuration/BillingType'
import CustomerPage from './containers/Configuration/Customer'

import TransactionPage from './containers/Payment/Transaction'
import PickupBillingPage from './containers/PickupBilling/Billing'

import PickupPage from './containers/PickupBilling/Pickup'

import BankPage from './containers/System/Bank'
import TruckPage from './containers/System/Truck'
import PaymentMethodPage from './containers/System/PaymentMethod'
import CountryPage from './containers/System/Country'
import StatePage from './containers/System/State'
import CurrencyPage from './containers/System/Currency'
import ZonePage from './containers/System/Zone'
import BranchPage from './containers/System/Branch'
import CommissionPage from './containers/System/Commission'
import ThemePage from './containers/System/Theme'

import { authenticatedPage } from './helpers/HelperMethods'

const routeOptions = [
  { component: authenticatedPage(DashboardPage), path: '/', exact: true },
  { component: SetPasswordPage, path: '/Set-password', exact: true },
  { component: ResetPasswordPage, path: '/Reset-password', exact: true },
  { component: ForgotPasswordPage, path: '/Forgot-password', exact: true },
  { component: ChangePasswordPage, path: '/ChangePassword', exact: true },
  { component: LoginPage, path: '/Login', exact: true },
  { component: authenticatedPage(UserMgtPage), path: '/Users', exact: true },
  { component: authenticatedPage(RoleMgtPage), path: '/Roles', exact: true },
  { component: authenticatedPage(TenantPage), path: '/Tenants', exact: true },
  { component: authenticatedPage(ClientPage), path: '/Clients', exact: true },
  {
    component: authenticatedPage(CustTypePage),
    path: '/Config/CustomerType',
    exact: true
  },
  {
    component: authenticatedPage(SubCustTypePage),
    path: '/Config/SubCustomerType',
    exact: true
  },
  {
    component: authenticatedPage(BillingTypePage),
    path: '/Config/BillingType',
    exact: true
  },
  {
    component: authenticatedPage(CustomerPage),
    path: '/Config/Customer',
    exact: true
  },
  {
    component: authenticatedPage(ThemePage),
    path: '/System/Theme',
    exact: true
  },
  {
    component: authenticatedPage(BankPage),
    path: '/System/Banks',
    exact: true
  },
  {
    component: authenticatedPage(TruckPage),
    path: '/System/Trucks',
    exact: true
  },
  {
    component: authenticatedPage(PaymentMethodPage),
    path: '/System/PaymentMethods',
    exact: true
  },
  {
    component: authenticatedPage(CountryPage),
    path: '/System/Country',
    exact: true
  },
  {
    component: authenticatedPage(StatePage),
    path: '/System/States',
    exact: true
  },
  {
    component: authenticatedPage(CurrencyPage),
    path: '/System/Currency',
    exact: true
  },
  {
    component: authenticatedPage(ZonePage),
    path: '/System/Zones',
    exact: true
  },
  {
    component: authenticatedPage(BranchPage),
    path: '/System/Branch',
    exact: true
  },
  {
    component: authenticatedPage(CommissionPage),
    path: '/System/Commission',
    exact: true
  },
  {
    component: authenticatedPage(TransactionPage),
    path: '/Payment/Transactions',
    exact: true
  },
  { component: authenticatedPage(PickupPage), path: '/Pickup', exact: true },
  {
    component: authenticatedPage(PickupBillingPage),
    path: '/Pickup/Billing',
    exact: true
  }
]

export default routeOptions
