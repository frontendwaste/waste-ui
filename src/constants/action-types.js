export const GET_USERS = 'GET_USERS'
export const TOGGLE_MODAL = 'TOGGLE_MODAL'
export const HANDLE_PAGING = 'HANDLE_PAGING'
export const LOAD_GRID_ITEM = 'LOAD_GRID_ITEM'
export const UPLOAD_DATA = 'UPLOAD_DATA'

export const FETCH_TENANT = 'FETCH_TENANT'
export const FETCH_CLIENT = 'FETCH_CLIENT'

export const FETCH_BANK = 'FETCH_BANK'
export const FETCH_BRANCH = 'FETCH_BRANCH'
export const FETCH_COMMISSION = 'FETCH_COMMISSION'
export const FETCH_COUNTRY = 'FETCH_COUNTRY'
export const FETCH_CURRENCY = 'FETCH_CURRENCY'
export const FETCH_PAYMENT_METHOD = 'FETCH_PAYMENT_METHOD'
export const FETCH_STATE = 'FETCH_STATE'
export const FETCH_THEME = 'FETCH_THEME'
export const FETCH_TRUCK = 'FETCH_TRUCK'
export const FETCH_ZONE = 'FETCH_ZONE'

export const FETCH_TRANSACTION = 'FETCH_TRANSACTION'
export const FETCH_CUSTOMER_WALLET = 'FETCH_CUSTOMER_WALLET'
export const FETCH_CHANNEL = 'FETCH_CHANNEL'
export const FETCH_PAYMENT_STATUS = 'FETCH_PAYMENT_STATUS'

export const FETCH_PICKUP = 'FETCH_PICKUP'
export const FETCH_PICKUP_STATUS = 'FETCH_PICKUP_STATUS'
export const FETCH_BILLING = 'FETCH_BILLING'

export const FETCH_CUSTOMER = 'FETCH_CUSTOMER'
export const FETCH_CUSTOMER_TYPE = 'FETCH_CUSTOMER_TYPE'
export const FETCH_SUB_CUSTOMER_TYPE = 'FETCH_SUB_CUSTOMER_TYPE'
export const FETCH_BILLING_TYPE = 'FETCH_BILLING_TYPE'

export const FETCH_ROLE = 'FETCH_ROLE'
export const FETCH_USER = 'FETCH_USER'
