export const BASE_URL = 'http://34.233.133.108:8000'

export const GET_CLIENT = BASE_URL + 'components/Tenancy/Clients/data.json'

export const GET_TENANT = BASE_URL + '/setup/api/v1/tenant'
export const UPDATE_TENANT = BASE_URL + '/setup/api/v1/tenant/'
export const CREATE_TENANT = BASE_URL + '/setup/api/v1/tenant'

export const GET_THEME = BASE_URL + '/setup/api/v1/theme'
export const UPDATE_THEME = BASE_URL + '/setup/api/v1/theme/'
export const CREATE_THEME = BASE_URL + '/setup/api/v1/theme'

export const GET_BANK = BASE_URL + '/setup/api/v1/bank'
export const UPDATE_BANK = BASE_URL + '/setup/api/v1/bank/'
export const CREATE_BANK = BASE_URL + '/setup/api/v1/bank'

export const GET_TRUCK = BASE_URL + '/setup/api/v1/truck'
export const UPDATE_TRUCK = BASE_URL + '/setup/api/v1/truck/'
export const CREATE_TRUCK = BASE_URL + '/setup/api/v1/truck'

export const GET_PAYMENT_METHOD = BASE_URL + '/setup/api/v1/payment'
export const UPDATE_PAYMENT_METHOD = BASE_URL + '/setup/api/v1/payment/'
export const CREATE_PAYMENT_METHOD = BASE_URL + '/setup/api/v1/payment'

export const GET_COUNTRY = BASE_URL + '/setup/api/v1/country'
export const UPDATE_COUNTRY = BASE_URL + '/setup/api/v1/country/'
export const CREATE_COUNTRY = BASE_URL + '/setup/api/v1/country'

export const GET_STATE = BASE_URL + '/setup/api/v1/states'
export const UPDATE_STATE = BASE_URL + '/setup/api/v1/states/'
export const CREATE_STATE = BASE_URL + '/setup/api/v1/states'

export const GET_CURRENCY = BASE_URL + '/setup/api/v1/currency'
export const UPDATE_CURRENCY = BASE_URL + '/setup/api/v1/currency/'
export const CREATE_CURRENCY = BASE_URL + '/setup/api/v1/currency'

export const GET_ZONE = BASE_URL + '/setup/api/v1/zone'
export const UPDATE_ZONE = BASE_URL + '/setup/api/v1/zone/'
export const CREATE_ZONE = BASE_URL + '/setup/api/v1/zone'

export const GET_BRANCH = BASE_URL + '/setup/api/v1/branch'
export const UPDATE_BRANCH = BASE_URL + '/setup/api/v1/branch/'
export const CREATE_BRANCH = BASE_URL + '/setup/api/v1/branch'

export const GET_COMMISSION = BASE_URL + '/setup/api/v1/commission'
export const UPDATE_COMMISSION = BASE_URL + '/setup/api/v1/commission/'
export const CREATE_COMMISSION = BASE_URL + '/setup/api/v1/commission'

export const GET_CUSTOMER_TYPE = BASE_URL + '/customer/customertype'
export const UPDATE_CUSTOMER_TYPE = BASE_URL + '/customer/customertype/'
export const CREATE_CUSTOMER_TYPE = BASE_URL + '/customer/customertype'

export const GET_SUB_CUSTOMER_TYPE = BASE_URL + '/customer/subcustomertype'
export const UPDATE_SUB_CUSTOMER_TYPE = BASE_URL + '/customer/subcustomertype/'
export const CREATE_SUB_CUSTOMER_TYPE = BASE_URL + '/customer/subcustomertype'

export const GET_BILLING_TYPE = BASE_URL + '/customer/billingtype'
export const UPDATE_BILLING_TYPE = BASE_URL + '/customer/billingtype/'
export const CREATE_BILLING_TYPE = BASE_URL + '/customer/billingtype'

export const GET_TRANSACTIONS = BASE_URL + '/payment/transactions'
export const CREATE_TRANSACTION = BASE_URL + '/payment/transaction'
export const VALIDATE_TRANSACTION = BASE_URL + '/payment/transaction/validate'
export const VOID_TRANSACTION = BASE_URL + '/payment/transaction/void'
export const SEARCH_TRANSACTIONS = BASE_URL + '/payment/transactions/search'
export const GET_CHANNEL = BASE_URL + '/payment/channel'
export const GET_PAYMENT_STATUS = BASE_URL + '/payment/status'

export const GET_WALLETS = BASE_URL + '/payment/wallets'
export const GET_WALLET = BASE_URL + '/payment/wallet/user'

export const SEARCH_PICKUP = BASE_URL + '/pickup-billing/pickups/search'
export const CREATE_PICKUP = BASE_URL + '/pickup-billing/pickup'
export const UPDATE_PICKUP = BASE_URL + '/pickup-billing/pickup/'
export const GET_BILLING = BASE_URL + '/pickup-billing/billings/search'

export const GET_PICKUP_STATUS = BASE_URL + '/pickup-billing/status'
export const CREATE_PICKUP_STATUS = BASE_URL + '/pickup-billing/status'
export const UPDATE_PICKUP_STATUS = BASE_URL + '/pickup-billing/status/'

export const GET_CUSTOMER = BASE_URL + '/customer/customers'
export const GET_CUSTOMER_TEMPLATE =
  BASE_URL + '/customer/customers/download/template'
export const SEARCH_CUSTOMER = BASE_URL + '/customer/customers/search'
export const UPDATE_CUSTOMER = BASE_URL + '/customer/customer/'
export const CREATE_CUSTOMER = BASE_URL + '/customer/customer'
export const UPLOAD_CUSTOMER = BASE_URL + '/customer/customers/upload'

export const GET_USER = BASE_URL + '/usermgt/api/v1/user'
export const UPDATE_USER = BASE_URL + '/usermgt/api/v1/user/'
export const CREATE_USER = BASE_URL + '/usermgt/api/v1/user'

export const GET_ROLE = BASE_URL + '/usermgt/api/v1/role'
export const UPDATE_ROLE = BASE_URL + '/usermgt/api/v1/role/'
export const CREATE_ROLE = BASE_URL + '/usermgt/api/v1/role'

export const LOGIN =
  'https://www.vuefy.com/auth/realms/WasteCo/protocol/openid-connect/token'
export const SET_PASSWORD = BASE_URL + '/usermgt/api/v1/user/verify-email'
export const RESET_PASSWORD = BASE_URL + '/usermgt/api/v1/user/reset-password'
export const FORGOT_PASSWORD = BASE_URL + '/usermgt/api/v1/user/forgot-password'
export const CHANGE_PASSWORD = BASE_URL + '/usermgt/api/v1/user/change-password'
// export const CHANGE_PASSWORD = id =>
//   `/usermgt/api/v1/user/${id}/change-password`
