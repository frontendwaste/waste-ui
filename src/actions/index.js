import {
  TOGGLE_MODAL,
  HANDLE_PAGING,
  LOAD_GRID_ITEM,
  UPLOAD_DATA
} from '../constants/action-types'
import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import { showLoading, hideLoading } from 'react-redux-loading-bar'
import history from '../history'

const handleLogOut = history => {
  localStorage.clear()
  history.replace({
    pathname: '/login'
  })
}

const beforeSend = (dispatch, config) => {
  dispatch(showLoading())
  const access_token = localStorage.getItem('token')

  if (access_token) {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token
  } else {
    axios.defaults.headers.common.Authorization = null
  }
  if (config) {
    if (config.Authorization) {
      axios.defaults.headers.common.Authorization = config.Authorization
    }
  }
}

const handleSuccess = (result, dispatch, actionType) => {
  dispatch(hideLoading())
  console.log(actionType, result.data)

  if (actionType) {
    dispatch(requestDataSuccess(result, actionType))
  } else {
    if (result.data) {
      if (result.data.data && typeof result.data.data === 'string') {
        toastr.success('Success', result.data)
      } else if (result.data.error && typeof result.data.error === 'string') {
        toastr.error('Error', result.data.error)
      }
      // else {
      //   toastr.success('Success', 'Submitted Successfully')
      // }
    }
    dispatch(toggleModal('', true))
  }
}

const showErrors = data => {
  if (typeof data === 'string') {
    return data
  } else {
    if (data.error) {
      if (typeof data.error === 'string') {
        return data.error
      } else if (data.error.length > 0) {
        return data.error[0]
      }
    } else if (data.status_description) {
      return data.status_description
    } else if (data.error_description) {
      return data.error_description
    } else {
      return 'An error occurred'
    }
  }
}

const handleError = (error, dispatch) => {
  dispatch(hideLoading())
  console.dir(error)

  if (error.response) {
    const { status, data } = error.response

    if (status === 401) {
      toastr.error('Error', 'Session has expired. Login again')
      handleLogOut(history)
    } else if (status === 403) {
      toastr.error('Error', 'You are not authorized to carry out this action')
      history.replace({ pathname: '/' })
    } else {
      toastr.error('Error', showErrors(data))
    }
  } else {
    console.dir(error)
  }
}

export const toggleModal = (action, visible) => {
  return {
    type: TOGGLE_MODAL,
    payload: {
      action: action || '',
      visible: !visible
    }
  }
}

export const handlePaging = ({ currPage, noOfPages, pageSize }) => {
  return {
    type: HANDLE_PAGING,
    payload: {
      currPage,
      noOfPages,
      pageSize
    }
  }
}

export const loadGridItem = (id, items, item) => {
  return {
    type: LOAD_GRID_ITEM,
    payload: item ? item : items.find(item => item.id === id)
  }
}

export const uploadingData = data => {
  return { type: UPLOAD_DATA, payload: data }
}

export const requestDataSuccess = (response, type) => {
  return {
    type,
    payload: response
  }
}

export function getData(url, actionType, config) {
  return function(dispatch) {
    beforeSend(dispatch, config)

    return axios({
      method: 'get',
      url,
      ...config
    })
      .then(res => {
        handleSuccess(res, dispatch, actionType)
        return res
      })
      .catch(function(error) {
        handleError(error, dispatch)
      })
  }
}

export function postData(url, data, config) {
  return function(dispatch) {
    beforeSend(dispatch, config)

    return axios({
      method: 'post',
      url,
      data,
      ...config
    })
      .then(res => {
        handleSuccess(res, dispatch)
        return res
      })
      .catch(function(error) {
        handleError(error, dispatch)
      })
  }
}

export function putData(url, data, config) {
  return function(dispatch) {
    beforeSend(dispatch, config)

    return axios({
      method: 'put',
      url,
      data,
      ...config
    })
      .then(res => {
        handleSuccess(res, dispatch)
        return res
      })
      .catch(function(error) {
        handleError(error, dispatch)
      })
  }
}
