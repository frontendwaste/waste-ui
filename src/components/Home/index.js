import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Layout from '../Shared/Layout'

class Dashboard extends Component {
  render() {
    const { history } = this.props

    return (
      <Layout history={history}>
        <h3 className={'pv2'}>Overview</h3>
      </Layout>
    )
  }
}

Dashboard.propTypes = {
  history: PropTypes.object.isRequired
}

export default Dashboard
