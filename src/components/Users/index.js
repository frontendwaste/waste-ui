import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Modal } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../Shared/Layout'
import FormWrapper from '../Shared/FormWrapper'
import fieldValidation from '../../helpers/FieldValidation'

import UserGrid from './userGrid'
import { formFields, searchFormFields } from './fieldSchema'

const Users = props => {
  const {
      history,
      modalVisible,
      modalActionName,
      selectedUser,
      formSubmit,
      searchFormSubmit,
      modalToggle,
      tenantId
    } = props,
    initialValues =
      (modalActionName === 'edit' && {
        initialValues: {
          ...selectedUser,
          mobileNumber:
            (!!selectedUser.attributes &&
              selectedUser.attributes.MobileNumber[0]) ||
            '',
          alternativeMobileNumber:
            (!!selectedUser.attributes &&
              selectedUser.attributes.MobileNumber[1]) ||
            '',
          roleId: (!!selectedUser.role && selectedUser.role.id) || '',
          tenantId: tenantId
            ? tenantId
            : (!!selectedUser.attributes &&
                selectedUser.attributes.tenantId[0]) ||
              ''
        }
      }) ||
      {}

  return (
    <Fragment>
      <Layout history={history}>
        <Card fluid>
          <Card.Content>
            <Card.Header>Existing Users</Card.Header>
            <Button
              basic
              compact
              floated="right"
              onClick={() => modalToggle('create', modalVisible)}
            >
              Create
            </Button>
          </Card.Content>
          <Card.Content>
            <Formik
              initialValues={{}}
              validationSchema={fieldValidation(searchFormFields)}
              onSubmit={searchFormSubmit}
              component={() => (
                <FormWrapper
                  fields={searchFormFields}
                  button={[
                    {
                      title: 'Search',
                      className: 'bg-primary-color',
                      type: 'submit'
                    }
                  ]}
                />
              )}
            />
          </Card.Content>
          <Card.Content>
            <UserGrid {...props} />
          </Card.Content>
        </Card>
      </Layout>

      <Modal
        onClose={() => modalToggle('', modalVisible)}
        open={modalVisible}
        size="large"
        closeIcon
      >
        <Modal.Header>{modalActionName} User</Modal.Header>
        <Modal.Content>
          <Formik
            {...initialValues}
            validationSchema={fieldValidation(formFields(props))}
            onSubmit={formSubmit}
            component={() => (
              <FormWrapper
                fields={formFields(props)}
                button={[
                  {
                    title: modalActionName,
                    className: 'bg-primary-color',
                    type: 'submit'
                  }
                ]}
              />
            )}
          />
        </Modal.Content>
      </Modal>
    </Fragment>
  )
}

Users.propTypes = {
  history: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  modalActionName: PropTypes.string.isRequired,
  tenantId: PropTypes.string,
  usersData: PropTypes.object.isRequired,
  rolesData: PropTypes.object.isRequired,
  selectedUser: PropTypes.object.isRequired,
  fetchGridData: PropTypes.func.isRequired,
  formSubmit: PropTypes.func.isRequired,
  searchFormSubmit: PropTypes.func.isRequired,
  modalToggle: PropTypes.func.isRequired,
  actionClick: PropTypes.func.isRequired
}

export default Users
