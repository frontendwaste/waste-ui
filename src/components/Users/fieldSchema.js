import FormInputs from '../Shared/FormInputs'
import FormSelect from '../Shared/FormInputs/select'

export const formFields = ({
  rolesData,
  modalActionName,
  tenantId,
  tenantsData
}) => {
  let fields = [
    {
      name: 'roleId',
      options: (!!rolesData && rolesData.data) || [],
      label: 'Role',
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      type: 'email',
      name: 'email',
      label: 'Email',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      name: 'firstName',
      label: 'First Name',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      name: 'lastName',
      label: 'Last Name',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      pattern: '[0-9]*',
      name: 'mobileNumber',
      label: 'Mobile Number',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      pattern: '[0-9]*',
      name: 'alternativeMobileNumber',
      label: 'Alternative Mobile Number',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'checkbox',
      name: 'enabled',
      label: 'Enabled',
      component: FormInputs,
      required: false,
      width: 4
    }
  ]

  if (modalActionName === 'create') {
    fields = [
      {
        type: 'text',
        name: 'username',
        label: 'Username',
        component: FormInputs,
        required: true,
        width: 4
      },
      ...fields
    ]
  }

  if (!tenantId) {
    fields = [
      {
        name: 'tenantId',
        options: (!!tenantsData && tenantsData.records) || [],
        label: 'Tenant',
        component: FormSelect,
        required: true,
        width: 4
      },
      ...fields
    ]
  }

  return fields
}

export const searchFormFields = [
  {
    type: 'text',
    name: 'search',
    label: 'Search All',
    component: FormInputs,
    required: false,
    width: 4
  },
  {
    type: 'text',
    name: 'firstName',
    label: 'First Name',
    component: FormInputs,
    required: false,
    width: 4
  },
  {
    type: 'text',
    name: 'lastName',
    label: 'Last Name',
    component: FormInputs,
    required: false,
    width: 4
  },
  {
    type: 'text',
    name: 'username',
    label: 'Username',
    component: FormInputs,
    required: false,
    width: 4
  },
  {
    type: 'email',
    name: 'email',
    label: 'Email',
    component: FormInputs,
    required: false,
    width: 4
  }
]
