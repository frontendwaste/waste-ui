import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import GridWrapper from '../Shared/GridWrapper'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Edit User"
        onClick={() => actionClick(row.id, 'edit', data, modalVisible)}
      >
        Edit
      </Button>
    </Fragment>
  )
}

const UserGrid = ({
  usersData: { data },
  actionClick,
  modalVisible,
  fetchGridData,
  pagination
}) => {
  return (
    <GridWrapper
      columns={[
        {
          Header: 'Full Name',
          accessor: 'fullName',
          Cell: ({ original }) => `${original.firstName} ${original.lastName}`
        },
        {
          Header: 'Email',
          accessor: 'email'
        },
        {
          Header: 'Username',
          accessor: 'username'
        },
        {
          Header: 'Option',
          accessor: 'id',
          Cell: cellProps =>
            composeCell(cellProps, {
              actionClick,
              data: data.Records,
              modalVisible
            })
        }
      ]}
      data={(!!data && data.Records) || []}
      totalRecords={parseInt((!!data && data.Size) || 0, 10)}
      pagination={pagination}
      fetchData={fetchGridData}
    />
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

UserGrid.propTypes = {
  usersData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired,
  fetchGridData: PropTypes.func.isRequired
}

export default UserGrid
