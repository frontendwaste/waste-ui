import FormInputs from '../../Shared/FormInputs'
import FormSelect from '../../Shared/FormInputs/select'

export const formFields = ({ countryData }) => {
  return [
    {
      type: 'text',
      name: 'name',
      label: 'Name',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      name: 'state_code',
      label: 'Code',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      name: 'country_id',
      options: (!!countryData && countryData.records) || [],
      label: 'Country',
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      type: 'checkbox',
      name: 'status',
      label: 'Enabled',
      component: FormInputs,
      required: false,
      width: 4
    }
  ]
}
