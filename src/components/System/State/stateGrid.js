import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import GridWrapper from '../../Shared/GridWrapper'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Edit State"
        onClick={() => actionClick(row.id, 'edit', data, modalVisible)}
      >
        Edit
      </Button>
    </Fragment>
  )
}

const modCellHtml = ({ value }) => {
  const labelClass = `label ${
    value === 'enable' ? 'label-success' : 'label-danger'
  }`

  return <span className={labelClass}>{value}</span>
}

const StateGrid = props => {
  const {
    statesData,
    countryData,
    actionClick,
    modalVisible,
    fetchGridData,
    pagination
  } = props

  return (
    <GridWrapper
      columns={
        (!!countryData.records &&
          !!countryData.records.length && [
          {
            Header: 'Name',
            accessor: 'name'
          },
          {
            Header: 'Code',
            accessor: 'state_code'
          },
          {
            Header: 'Country',
            accessor: 'country_id',
            Cell: ({ value }) => {
              const country = countryData.records.find(
                type => type.id === value
              )
              return (!!country && country.name) || ''
            }
          },
          {
            Header: 'Status',
            accessor: 'status',
            Cell: modCellHtml
          },
          {
            Header: 'Option',
            accessor: 'id',
            Cell: cellProps =>
              composeCell(cellProps, {
                actionClick,
                data: statesData.records,
                modalVisible
              })
          }
        ]) ||
        []
      }
      data={statesData.records || []}
      totalRecords={parseInt(statesData.totalRecords || 0, 10)}
      pagination={pagination}
      fetchData={fetchGridData}
    />
    //)
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

modCellHtml.propTypes = {
  value: PropTypes.string.isRequired
}

StateGrid.propTypes = {
  statesData: PropTypes.object.isRequired,
  countryData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired,
  fetchGridData: PropTypes.func.isRequired
}

export default StateGrid
