import FormInputs from '../../Shared/FormInputs'
import FormSelect from '../../Shared/FormInputs/select'

export const formFields = ({ uploadingData, fonts }, setFieldValue) => [
  {
    type: 'color',
    name: 'color',
    label: 'Color',
    component: FormInputs,
    required: true,
    width: 4
  },
  {
    type: 'file',
    name: 'logo',
    label: 'Logo',
    component: FormInputs,
    onChange: event => {
      const file = event.currentTarget.files[0]
      setFieldValue('file', file)
      uploadingData(file)
    },
    required: false,
    multiple: false,
    width: 4
  },
  {
    name: 'fonts',
    options: fonts || [],
    label: 'Font',
    component: FormSelect,
    required: true,
    width: 4
  },
  {
    type: 'checkbox',
    name: 'status',
    label: 'Enabled',
    component: FormInputs,
    required: false,
    width: 4
  }
  // {
  //   type: 'text',
  //   name: 'templates',
  //   label: 'Template',
  //   component: FormInputs,
  //   required: true
  // }
]
