import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import GridWrapper from '../../Shared/GridWrapper'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Edit Branch"
        onClick={() => actionClick(row.id, 'edit', data, modalVisible)}
      >
        Edit
      </Button>
    </Fragment>
  )
}

const modCellHtml = ({ value }) => {
  const labelClass = `label ${
    value === 'enable' ? 'label-success' : 'label-danger'
  }`

  return <span className={labelClass}>{value}</span>
}

const BranchGrid = ({
  branchData,
  stateData,
  countryData,
  actionClick,
  modalVisible,
  fetchGridData,
  pagination
}) => {
  return (
    <GridWrapper
      columns={
        (!!stateData.records &&
          !!stateData.records.length &&
          !!countryData.records &&
          !!countryData.records.length && [
          {
            Header: 'Name',
            accessor: 'name'
          },
          {
            Header: 'State',
            accessor: 'states_id',
            Cell: ({ value }) => {
              const state = stateData.records.find(type => type.id === value)

              return (!!state && state.name) || ''
            }
          },
          {
            Header: 'Country',
            accessor: 'states_id',
            Cell: ({ value }) => {
              const country = countryData.records.find(
                country =>
                  country.id ===
                    stateData.records.find(state => state.id === value)
                      .country_id
              )

              return (!!country && country.name) || ''
            }
          },
          {
            Header: 'Status',
            accessor: 'status',
            Cell: modCellHtml
          },
          {
            Header: 'Option',
            accessor: 'id',
            Cell: cellProps =>
              composeCell(cellProps, {
                actionClick,
                data: branchData.records,
                modalVisible
              })
          }
        ]) ||
        []
      }
      data={branchData.records || []}
      totalRecords={parseInt(branchData.totalRecords, 10)}
      pagination={pagination}
      fetchData={fetchGridData}
    />
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

modCellHtml.propTypes = {
  value: PropTypes.string.isRequired
}

BranchGrid.propTypes = {
  branchData: PropTypes.object.isRequired,
  stateData: PropTypes.object.isRequired,
  countryData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired,
  fetchGridData: PropTypes.func.isRequired
}

export default BranchGrid
