import FormInputs from '../../Shared/FormInputs'
import FormSelect from '../../Shared/FormInputs/select'

export const formFields = (
  { countryData, stateDataFiltered, getState },
  setFieldValue
) => {
  return [
    {
      type: 'text',
      name: 'name',
      label: 'Name',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      name: 'country_id',
      options: countryData.records || [],
      label: 'Country',
      onChange: e => {
        const countryId = parseInt(e.target.value, 10)
        getState(countryId)
        setFieldValue('country_id', countryId)
      },
      onBlur: e => {
        console.dir(e.target)
      },
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      name: 'states_id',
      options: stateDataFiltered || [],
      label: 'State',
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      type: 'checkbox',
      name: 'status',
      label: 'Enabled',
      component: FormInputs,
      required: false,
      width: 4
    }
  ]
}
