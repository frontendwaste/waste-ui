import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'
import ReactTable from 'react-table'

import 'react-table/react-table.css'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Edit Commission"
        onClick={() => actionClick(row.id, 'edit', data, modalVisible)}
      >
        Edit
      </Button>
    </Fragment>
  )
}

const modCellHtml = ({ value }) => {
  const labelClass = `label ${
    value === 'enable' ? 'label-success' : 'label-danger'
  }`

  return <span className={labelClass}>{value}</span>
}

const CommissionGrid = props => {
  return (
    <Fragment>
      <ReactTable
        columns={[
          {
            Header: 'Creation Date',
            accessor: 'created_at'
          },
          {
            Header: 'Name',
            accessor: 'name'
          },
          {
            Header: 'Status',
            accessor: 'status',
            Cell: modCellHtml
          },
          {
            Header: 'Option',
            accessor: 'id',
            Cell: cellProps => composeCell(cellProps, props)
          }
        ]}
        data={props.data}
        defaultPageSize={10}
        className="gilroymedium"
      />
    </Fragment>
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

modCellHtml.propTypes = {
  value: PropTypes.string.isRequired
}

CommissionGrid.propTypes = {
  data: PropTypes.array.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired
}

export default CommissionGrid
