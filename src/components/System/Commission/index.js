import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Modal } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import CommissionGrid from './commissionGrid'
import { formFields } from './fieldSchema'

const Commission = props => {
  const {
      history,
      modalVisible,
      modalActionName,
      commissionData,
      selectedCommission,
      formSubmit,
      actionClick,
      modalToggle
    } = props,
    initialValues =
      (modalActionName === 'edit' && {
        initialValues: {
          ...selectedCommission,
          status: selectedCommission.status === 'enable' ? true : false
        }
      }) ||
      {}

  return (
    <Fragment>
      <Layout history={history}>
        {commissionData.records && (
          <Card fluid>
            <Card.Content>
              <Card.Header>Existing Commissions</Card.Header>
              <Button
                basic
                compact
                floated="right"
                onClick={() => modalToggle('create', modalVisible)}
              >
                Create
              </Button>
            </Card.Content>
            <Card.Content>
              <CommissionGrid
                data={commissionData.records || []}
                actionClick={actionClick}
                modalVisible={modalVisible}
              />
            </Card.Content>
          </Card>
        )}
      </Layout>

      <Modal
        onClose={() => modalToggle('', modalVisible)}
        open={modalVisible}
        size="small"
        closeIcon
      >
        <Modal.Header>{modalActionName} Commission</Modal.Header>
        <Modal.Content>
          <Formik
            {...initialValues}
            validationSchema={fieldValidation(formFields)}
            onSubmit={formSubmit}
            component={() => (
              <FormWrapper fields={formFields} action={modalActionName} />
            )}
          />
        </Modal.Content>
      </Modal>
    </Fragment>
  )
}

Commission.propTypes = {
  history: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  modalActionName: PropTypes.string.isRequired,
  commissionData: PropTypes.object.isRequired,
  selectedCommission: PropTypes.object.isRequired,
  formSubmit: PropTypes.func.isRequired,
  modalToggle: PropTypes.func.isRequired,
  actionClick: PropTypes.func.isRequired
}

export default Commission
