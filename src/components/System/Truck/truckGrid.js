import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import GridWrapper from '../../Shared/GridWrapper'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Edit Truck"
        onClick={() => actionClick(row.id, 'edit', data, modalVisible)}
      >
        Edit
      </Button>
    </Fragment>
  )
}

const modCellHtml = ({ value }) => {
  const labelClass = `label ${
    value === 'enable' ? 'label-success' : 'label-danger'
  }`

  return <span className={labelClass}>{value}</span>
}

const TruckGrid = ({
  trucksData: { records, totalRecords },
  actionClick,
  modalVisible,
  fetchGridData,
  pagination
}) => {
  return (
    <GridWrapper
      columns={[
        {
          Header: 'Driver Name',
          accessor: 'drivername'
        },
        {
          Header: 'Plate Number',
          accessor: 'platenumber'
        },
        {
          Header: 'Capacity',
          accessor: 'capacity'
        },
        {
          Header: 'Status',
          accessor: 'status',
          Cell: modCellHtml
        },
        {
          Header: 'Option',
          accessor: 'id',
          Cell: cellProps =>
            composeCell(cellProps, {
              actionClick,
              data: records,
              modalVisible
            })
        }
      ]}
      data={records || []}
      totalRecords={parseInt(totalRecords || 0, 10)}
      pagination={pagination}
      fetchData={fetchGridData}
    />
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

modCellHtml.propTypes = {
  value: PropTypes.string.isRequired
}

TruckGrid.propTypes = {
  trucksData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired,
  fetchGridData: PropTypes.func.isRequired
}

export default TruckGrid
