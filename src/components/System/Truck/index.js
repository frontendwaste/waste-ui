import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Modal } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import TruckGrid from './truckGrid'
import { formFields } from './fieldSchema'

const Truck = props => {
  const {
      history,
      modalVisible,
      modalActionName,
      selectedTruck,
      formSubmit,
      modalToggle
    } = props,
    initialValues =
      (modalActionName === 'edit' && {
        initialValues: {
          ...selectedTruck,
          status: selectedTruck.status === 'enable' ? true : false
        }
      }) ||
      {}

  return (
    <Fragment>
      <Layout history={history}>
        <Card fluid>
          <Card.Content>
            <Card.Header>Existing Trucks</Card.Header>
            <Button
              basic
              compact
              floated="right"
              onClick={() => modalToggle('create', modalVisible)}
            >
              Create
            </Button>
          </Card.Content>
          <Card.Content>
            <TruckGrid {...props} />
          </Card.Content>
        </Card>
      </Layout>

      <Modal
        onClose={() => modalToggle('', modalVisible)}
        open={modalVisible}
        size="large"
        closeIcon
      >
        <Modal.Header>{modalActionName} Truck</Modal.Header>
        <Modal.Content>
          <Formik
            {...initialValues}
            validationSchema={fieldValidation(formFields(props))}
            onSubmit={formSubmit}
            component={() => (
              <FormWrapper
                fields={formFields(props)}
                button={[
                  {
                    title: modalActionName,
                    className: 'bg-primary-color',
                    type: 'submit'
                  }
                ]}
              />
            )}
          />
        </Modal.Content>
      </Modal>
    </Fragment>
  )
}

Truck.propTypes = {
  history: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  modalActionName: PropTypes.string.isRequired,
  trucksData: PropTypes.object.isRequired,
  branchData: PropTypes.object.isRequired,
  selectedTruck: PropTypes.object.isRequired,
  fetchGridData: PropTypes.func.isRequired,
  formSubmit: PropTypes.func.isRequired,
  modalToggle: PropTypes.func.isRequired,
  actionClick: PropTypes.func.isRequired
}

export default Truck
