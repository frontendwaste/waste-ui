import FormInputs from '../../Shared/FormInputs'
import FormSelect from '../../Shared/FormInputs/select'

export const formFields = ({ branchData }) => {
  return [
    {
      type: 'text',
      name: 'drivername',
      label: 'Driver Name',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'number',
      name: 'capacity',
      label: 'Capacity',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      name: 'platenumber',
      label: 'Plate Number',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      name: 'phone',
      label: 'Phone Number',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      name: 'branch_id',
      options: (!!branchData && branchData.records) || [],
      label: 'Branch',
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      type: 'checkbox',
      name: 'status',
      label: 'Enabled',
      component: FormInputs,
      required: false,
      width: 4
    }
  ]
}
