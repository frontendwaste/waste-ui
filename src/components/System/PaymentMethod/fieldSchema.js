import FormInputs from '../../Shared/FormInputs'
import FormSelect from '../../Shared/FormInputs/select'

export const formFields = ({ branchData }) => {
  return [
    {
      type: 'text',
      name: 'name',
      label: 'Name',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      name: 'code',
      label: 'Code',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      name: 'branch_id',
      options: (!!branchData && branchData.records) || [],
      label: 'Branch',
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      type: 'checkbox',
      name: 'type',
      label: 'Online',
      component: FormInputs,
      required: false,
      width: 2
    },
    {
      type: 'checkbox',
      name: 'status',
      label: 'Enabled',
      component: FormInputs,
      required: false,
      width: 2
    }
  ]
}
