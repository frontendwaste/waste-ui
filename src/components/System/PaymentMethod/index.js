import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Modal } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import PaymentMethodGrid from './paymentMethodGrid'
import { formFields } from './fieldSchema'

const PaymentMethod = props => {
  const {
      history,
      modalVisible,
      modalActionName,
      selectedPaymentMethod,
      formSubmit,
      modalToggle
    } = props,
    initialValues =
      (modalActionName === 'edit' && {
        initialValues: {
          ...selectedPaymentMethod,
          type: selectedPaymentMethod.type === 'online' ? true : false,
          status: selectedPaymentMethod.status === 'enable' ? true : false
        }
      }) ||
      {}

  return (
    <Fragment>
      <Layout history={history}>
        <Card fluid>
          <Card.Content>
            <Card.Header>Existing Payment Methods</Card.Header>
            <Button
              basic
              compact
              floated="right"
              onClick={() => modalToggle('create', modalVisible)}
            >
              Create
            </Button>
          </Card.Content>
          <Card.Content>
            <PaymentMethodGrid {...props} />
          </Card.Content>
        </Card>
      </Layout>

      <Modal
        onClose={() => modalToggle('', modalVisible)}
        open={modalVisible}
        size="large"
        closeIcon
      >
        <Modal.Header>{modalActionName} Payment Method</Modal.Header>
        <Modal.Content>
          <Formik
            {...initialValues}
            validationSchema={fieldValidation(formFields(props))}
            onSubmit={formSubmit}
            component={() => (
              <FormWrapper
                fields={formFields(props)}
                button={[
                  {
                    title: modalActionName,
                    className: 'bg-primary-color',
                    type: 'submit'
                  }
                ]}
              />
            )}
          />
        </Modal.Content>
      </Modal>
    </Fragment>
  )
}

PaymentMethod.propTypes = {
  history: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  modalActionName: PropTypes.string.isRequired,
  paymentMethodsData: PropTypes.object.isRequired,
  branchData: PropTypes.object.isRequired,
  selectedPaymentMethod: PropTypes.object.isRequired,
  fetchGridData: PropTypes.func.isRequired,
  formSubmit: PropTypes.func.isRequired,
  modalToggle: PropTypes.func.isRequired,
  actionClick: PropTypes.func.isRequired
}

export default PaymentMethod
