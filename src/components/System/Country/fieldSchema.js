import FormInputs from '../../Shared/FormInputs'

export const formFields = [
  {
    type: 'text',
    name: 'name',
    label: 'Name',
    component: FormInputs,
    required: true,
    width: 6
  },
  {
    type: 'checkbox',
    name: 'status',
    label: 'Enabled',
    component: FormInputs,
    required: false,
    width: 4
  }
]
