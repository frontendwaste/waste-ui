import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Modal } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import CountryGrid from './countryGrid'
import { formFields } from './fieldSchema'

const Country = props => {
  const {
      history,
      modalVisible,
      modalActionName,
      selectedCountry,
      formSubmit,
      modalToggle
    } = props,
    initialValues =
      (modalActionName === 'edit' && {
        initialValues: {
          ...selectedCountry,
          status: selectedCountry.status === 'enable' ? true : false
        }
      }) ||
      {}

  return (
    <Fragment>
      <Layout history={history}>
        <Card fluid>
          <Card.Content>
            <Card.Header>Existing Countries</Card.Header>
            <Button
              basic
              compact
              floated="right"
              onClick={() => modalToggle('create', modalVisible)}
            >
              Create
            </Button>
          </Card.Content>
          <Card.Content>
            <CountryGrid {...props} />
          </Card.Content>
        </Card>
      </Layout>

      <Modal
        onClose={() => modalToggle('', modalVisible)}
        open={modalVisible}
        size="small"
        closeIcon
      >
        <Modal.Header>{modalActionName} Country</Modal.Header>
        <Modal.Content>
          <Formik
            {...initialValues}
            validationSchema={fieldValidation(formFields)}
            onSubmit={formSubmit}
            component={() => (
              <FormWrapper
                fields={formFields}
                button={[
                  {
                    title: modalActionName,
                    className: 'bg-primary-color',
                    type: 'submit'
                  }
                ]}
              />
            )}
          />
        </Modal.Content>
      </Modal>
    </Fragment>
  )
}

Country.propTypes = {
  history: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  modalActionName: PropTypes.string.isRequired,
  countryData: PropTypes.object.isRequired,
  selectedCountry: PropTypes.object.isRequired,
  fetchGridData: PropTypes.func.isRequired,
  formSubmit: PropTypes.func.isRequired,
  modalToggle: PropTypes.func.isRequired,
  actionClick: PropTypes.func.isRequired
}

export default Country
