import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import GridWrapper from '../../Shared/GridWrapper'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Edit Zone"
        onClick={() => actionClick(row.id, 'edit', data, modalVisible)}
      >
        Edit
      </Button>
    </Fragment>
  )
}

const modCellHtml = ({ value }) => {
  const labelClass = `label ${
    value === 'enable' ? 'label-success' : 'label-danger'
  }`

  return <span className={labelClass}>{value}</span>
}

const ZoneGrid = ({
  zonesData,
  actionClick,
  modalVisible,
  fetchGridData,
  pagination
}) => {
  return (
    <GridWrapper
      columns={[
        {
          Header: 'Name',
          accessor: 'name'
        },
        {
          Header: 'Code',
          accessor: 'code'
        },
        {
          Header: 'Status',
          accessor: 'status',
          Cell: modCellHtml
        },
        {
          Header: 'Option',
          accessor: 'id',
          Cell: cellProps =>
            composeCell(cellProps, {
              actionClick,
              data: zonesData.records,
              modalVisible
            })
        }
      ]}
      data={zonesData.records || []}
      totalRecords={parseInt(zonesData.totalRecords, 10)}
      pagination={pagination}
      fetchData={fetchGridData}
    />
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

modCellHtml.propTypes = {
  value: PropTypes.string.isRequired
}

ZoneGrid.propTypes = {
  zonesData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired,
  fetchGridData: PropTypes.func.isRequired
}

export default ZoneGrid
