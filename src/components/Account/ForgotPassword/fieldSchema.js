import FormInputs from '../../Shared/FormInputs'

export const formFields = [
  {
    type: 'email',
    name: 'email',
    label: 'Email',
    component: FormInputs,
    required: true
  }
]
