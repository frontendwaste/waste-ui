import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Formik } from 'formik'

import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import { formFields } from './fieldSchema'

const ForgotPassword = ({ formSubmit }) => {
  return (
    <Fragment>
      <div className="bg-white w-40 center flex flex-column pa5 ph6 br2 mt6">
        <h4 className="mb4">Forgot Password</h4>
        <Formik
          initialValues={{}}
          validationSchema={fieldValidation(formFields)}
          onSubmit={formSubmit}
          component={() => (
            <FormWrapper
              fields={formFields}
              action="Initiate Reset"
              grid={false}
            />
          )}
        />
        <Link to={'/Login'} className="mt3">
          Back to Login
        </Link>
      </div>
    </Fragment>
  )
}

ForgotPassword.propTypes = {
  formSubmit: PropTypes.func.isRequired
}

export default ForgotPassword
