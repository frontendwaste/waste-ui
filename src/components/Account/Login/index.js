import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { Formik } from 'formik'

import FormWrapper from '../../Shared/FormWrapper'
import AccountLayout from '../../Shared/Layout/account'
import fieldValidation from '../../../helpers/FieldValidation'

import { formFields } from './fieldSchema'

const Login = ({ formSubmit }) => {
  return (
    <AccountLayout>
      <h1 className="tc mb1 f2 f2-ns f1-l tracked-tight-m app-prim-color lato">
        Welcome Back
      </h1>
      <p className="tc mb5">Enter your details below.</p>
      <Formik
        initialValues={{}}
        validationSchema={fieldValidation(formFields)}
        onSubmit={formSubmit}
        component={() => (
          <FormWrapper
            fields={formFields}
            button={[
              {
                title: 'Sign In',
                className: 'bg-primary-color',
                type: 'submit',
                fluid: true
              }
            ]}
          />
        )}
      />
      <Link to={'/Forgot-password'} className="mt3">
        Forgot Password ?
      </Link>
    </AccountLayout>
  )
}

Login.propTypes = {
  formSubmit: PropTypes.func.isRequired
}

export default Login
