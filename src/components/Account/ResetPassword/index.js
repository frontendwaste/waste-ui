import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik'

import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import { formFields } from './fieldSchema'

const ResetPassword = ({ formSubmit }) => {
  return (
    <Fragment>
      <div className="bg-white w-40 center flex flex-column pa5 ph6 br2 mt6">
        <Formik
          initialValues={{}}
          validationSchema={fieldValidation(formFields)}
          onSubmit={formSubmit}
          component={() => (
            <FormWrapper
              fields={formFields}
              action="Reset Password"
              grid={false}
            />
          )}
        />
      </div>
    </Fragment>
  )
}

ResetPassword.propTypes = {
  formSubmit: PropTypes.func.isRequired
}

export default ResetPassword
