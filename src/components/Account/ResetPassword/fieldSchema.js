import FormInputs from '../../Shared/FormInputs'

export const formFields = [
  {
    type: 'password',
    name: 'password',
    label: 'Password',
    component: FormInputs,
    required: true
  },
  {
    type: 'password',
    name: 'confirmPassword',
    label: 'Confirm Password',
    component: FormInputs,
    required: true,
    equalto: 'password'
  }
]
