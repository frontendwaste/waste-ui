import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Card } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import { formFields } from './fieldSchema'

const ChangePassword = ({ history, formSubmit }) => {
  return (
    <Fragment>
      <Layout history={history}>
        <Card fluid>
          <Card.Content>
            <Card.Header>Change Password</Card.Header>
          </Card.Content>
          <Card.Content>
            <div className="w-40">
              <Formik
                initialValues={{}}
                validationSchema={fieldValidation(formFields)}
                onSubmit={formSubmit}
                component={() => (
                  <FormWrapper
                    fields={formFields}
                    button={[
                      {
                        title: 'Change Password',
                        className: 'bg-primary-color',
                        type: 'submit'
                      }
                    ]}
                    grid={false}
                  />
                )}
              />
            </div>
          </Card.Content>
        </Card>
      </Layout>
    </Fragment>
  )
}

ChangePassword.propTypes = {
  history: PropTypes.object.isRequired,
  formSubmit: PropTypes.func.isRequired
}

export default ChangePassword
