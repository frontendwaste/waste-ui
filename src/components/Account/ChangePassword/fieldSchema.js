import FormInputs from '../../Shared/FormInputs'

export const formFields = [
  {
    type: 'password',
    name: 'oldPassword',
    label: 'Old Password',
    component: FormInputs,
    required: true
  },
  {
    type: 'password',
    name: 'newPassword',
    label: 'New Password',
    component: FormInputs,
    required: true
  },
  {
    type: 'password',
    name: 'confirmPassword',
    label: 'Confirm New Password',
    component: FormInputs,
    required: true,
    equalto: {
      key: 'newPassword',
      name: 'New Password'
    }
  }
]
