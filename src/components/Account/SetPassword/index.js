import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik'

import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import { formFields } from './fieldSchema'

const SetPassword = ({ formSubmit }) => {
  return (
    <Fragment>
      <div className="bg-white w-40 center flex flex-column pa5 ph6 br2 mt6">
        <Formik
          initialValues={{}}
          validationSchema={fieldValidation(formFields)}
          onSubmit={formSubmit}
          component={() => (
            <FormWrapper
              fields={formFields}
              action="Set Password"
              grid={false}
            />
          )}
        />
      </div>
    </Fragment>
  )
}

SetPassword.propTypes = {
  formSubmit: PropTypes.func.isRequired
}

export default SetPassword
