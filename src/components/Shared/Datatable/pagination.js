import React from 'react'
import PropTypes from 'prop-types'
import { Icon, Label, Pagination, Table } from 'semantic-ui-react'

const PaginationWrapper = ({
  activePage,
  cols,
  totalPages,
  totalRecords,
  onPageChange
}) => (
  <Table.Footer>
    <Table.Row>
      <Table.HeaderCell colSpan={cols.length}>
        <Label basic floated="left" size="large">
          {totalRecords} Record(s) Found
        </Label>
        <Pagination
          floated="right"
          siblingRange={1}
          boundaryRange={1}
          firstItem={{ content: <Icon name="angle double left" />, icon: true }}
          lastItem={{ content: <Icon name="angle double right" />, icon: true }}
          prevItem={{ content: <Icon name="angle left" />, icon: true }}
          nextItem={{ content: <Icon name="angle right" />, icon: true }}
          activePage={activePage}
          totalPages={totalPages}
          onPageChange={onPageChange}
        />
      </Table.HeaderCell>
    </Table.Row>
  </Table.Footer>
)

PaginationWrapper.propTypes = {
  cols: PropTypes.array,
  totalRecords: PropTypes.number.isRequired,
  totalPages: PropTypes.number.isRequired,
  activePage: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired
}

export default PaginationWrapper
