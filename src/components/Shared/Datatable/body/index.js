import React from 'react'
import { Table } from 'semantic-ui-react'
import Rows from './rows'

const Body = props => (
  <Table.Body>
    <Rows {...props} />
  </Table.Body>
)

export default Body
