import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Table } from 'semantic-ui-react'
import Columns from './columns'

const Rows = props => {
  const { rows, cols } = props

  return (
    <Fragment>
      {!!rows.length && rows.length > 0 ? (
        rows.map((row, index) => (
          <Table.Row key={index} className="f6 dark-gray">
            {!!cols.length &&
              cols.map((col, index) => (
                <Columns key={index} col={col} row={row} />
              ))}
          </Table.Row>
        ))
      ) : (
        <Table.Row>
          <Table.Cell colSpan={cols.length} textAlign="center">
            <p className="pa2">No Data Found!</p>
          </Table.Cell>
        </Table.Row>
      )}
    </Fragment>
  )
}

Rows.propTypes = {
  cols: PropTypes.array.isRequired,
  rows: PropTypes.array.isRequired
}

export default Rows
