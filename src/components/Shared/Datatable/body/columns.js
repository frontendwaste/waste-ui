import React, { Fragment, Component } from 'react'
import PropTypes from 'prop-types'
import { Table } from 'semantic-ui-react'

class Columns extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: props.row.id,
      row: props.row,
      column: props.col,
      original: props.row,
      value: this.splitAccessor(props.row, props.col.accessor)
    }

    this.splitAccessor = this.splitAccessor.bind(this)
    this.composeValue = this.composeValue.bind(this)
  }

  componentWillMount() {
    const { column, row } = this.state

    this.setState({
      ...this.state,
      value: this.composeValue(row, column)
    })
  }

  splitAccessor(row, accessor) {
    const accessorArr = accessor.split('.').reduce((accum, item) => {
      return accum[item]
    }, row)

    return row[accessor] || accessorArr
  }

  composeValue(row, { Cell, accessor }) {
    const newValue = this.splitAccessor(row, accessor)
    const newState = { ...this.state, value: newValue }

    return Cell ? Cell(newState) : newValue
  }

  render() {
    const { value } = this.state

    return (
      <Fragment>
        <Table.Cell>{value}</Table.Cell>
      </Fragment>
    )
  }
}

Columns.propTypes = {
  col: PropTypes.object.isRequired,
  row: PropTypes.object.isRequired
}

export default Columns
