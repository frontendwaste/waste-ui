import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Dropdown, Grid, Input, Table } from 'semantic-ui-react'

import Header from './header'
import Body from './body/index'
import PaginationWrapper from './pagination'

class Datatable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      manual: false,
      pageSort: false,
      columns: [],
      data: [],
      pagedData: [],
      pageSize: 10,
      totalPages: 1,
      currPage: 1,
      noOfRecords: 0,
      dropDownOptions: [
        { key: 1, value: 5, text: '5 Rows' },
        { key: 2, value: 10, text: '10 Rows' },
        { key: 3, value: 20, text: '20 Rows' },
        { key: 4, value: 25, text: '25 Rows' },
        { key: 5, value: 50, text: '50 Rows' },
        { key: 6, value: 100, text: '100 Rows' }
      ],
      searchKey: ''
    }
    this.handlePageSizeChange = this.handlePageSizeChange.bind(this)
    this.handlePaginationChange = this.handlePaginationChange.bind(this)
  }

  componentDidMount() {
    const newState = this.morphProps(this.props)
    //eslint-disable-next-line
    this.setState(prevState => ({
      ...prevState,
      ...newState
    }))
  }

  componentDidUpdate(prevProps) {
    const { data, columns } = this.props

    if (data !== prevProps.data || columns !== prevProps.columns) {
      const newState = this.morphProps(this.props)
      //eslint-disable-next-line
      this.setState(prevState => ({
        ...prevState,
        ...newState
      }))
    }
  }

  morphProps({
    columns = [],
    data = [],
    pageSize = 10,
    manual = false,
    pageSort = false,
    page = 0,
    pages = 1,
    totalRecords = 0
  }) {
    const currPage = page + 1

    const startIndex = pageSize * currPage - pageSize

    const endIndex = startIndex + pageSize

    const pagedData = manual ? data : data.slice(startIndex, endIndex)

    const noOfRecords = manual ? totalRecords : data.length

    const totalPages = manual ? pages : Math.ceil(data.length / pageSize)

    return {
      columns,
      pageSize,
      data,
      manual,
      pageSort,
      pagedData,
      totalPages,
      currPage,
      noOfRecords
    }
  }

  handlePaginationChange(e, { activePage }) {
    const page = activePage - 1

    const { onPageChange, manual } = this.props

    const newState = this.morphProps({ ...this.state, page })

    if (manual) {
      onPageChange(page)
    } else {
      this.setState(prevState => ({
        ...prevState,
        ...newState
      }))
    }
  }

  handlePageSizeChange(e, { value }) {
    const page = 0

    const pageSize = value

    const { onPageSizeChange, manual } = this.props

    const newState = this.morphProps({ ...this.state, page, pageSize })

    if (manual) {
      onPageSizeChange(pageSize, page)
    } else {
      this.setState(prevState => ({
        ...prevState,
        ...newState
      }))
    }
  }

  // handleInputChange(evt) {
  //   const searchValue = evt.target.value.toLowerCase(),
  //     { pageSize, data } = this.props,
  //     pagedData = data.slice(0, pageSize)
  //   let newData = ''
  //   if (searchValue.trim() !== '') {
  //     newData = pagedData.filter(item => {
  //       for (let key in item) {
  //         let currObj = item[key].toString().toLowerCase()
  //         if (currObj.indexOf(searchValue) !== -1) {
  //           return true
  //         }
  //       }
  //     })
  //   } else {
  //     newData = pagedData
  //   }
  //   this.setState(prevState => {
  //     return {
  //       ...prevState,
  //       searchKey: searchValue,
  //       moddedData: newData
  //     }
  //   })
  // }

  render() {
    const { className, extras } = this.props
    const {
      pagedData,
      pageSize,
      totalPages,
      currPage,
      noOfRecords,
      searchKey,
      dropDownOptions,
      pageSort,
      columns
    } = this.state

    return (
      <Grid>
        <Grid.Row>
          <Grid.Column width={16}>
            {pageSort && (
              <Dropdown
                selection
                className="fr"
                defaultValue={pageSize}
                options={dropDownOptions}
                onChange={this.handlePageSizeChange}
              />
            )}
            <Input
              className="fl mb3"
              placeholder="Filter..."
              icon="search"
              value={searchKey}
              onChange={this.handleInputChange}
              iconPosition="left"
            />
            <Table className={className} {...extras}>
              <Header cols={columns} />
              <Body cols={columns} rows={pagedData} />
              <PaginationWrapper
                totalPages={totalPages}
                activePage={currPage}
                cols={columns}
                totalRecords={noOfRecords}
                onPageChange={this.handlePaginationChange}
              />
            </Table>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    )
  }
}

Datatable.propTypes = {
  columns: PropTypes.array.isRequired,
  data: PropTypes.array.isRequired,
  pageSize: PropTypes.number,
  page: PropTypes.number,
  pages: PropTypes.number,
  totalRecords: PropTypes.number,
  onPageChange: PropTypes.func,
  onPageSizeChange: PropTypes.func,
  manual: PropTypes.bool,
  pageSort: PropTypes.bool,
  className: PropTypes.string,
  extras: PropTypes.object
}

export default Datatable
