import React from 'react'
import PropTypes from 'prop-types'
import { Table } from 'semantic-ui-react'

const Header = ({ cols }) => (
  <Table.Header>
    <Table.Row>
      {!!cols.length &&
        cols.map((column, index) => (
          <Table.HeaderCell key={index}>{column.Header}</Table.HeaderCell>
        ))}
    </Table.Row>
  </Table.Header>
)

Header.propTypes = {
  cols: PropTypes.array.isRequired
}

export default Header
