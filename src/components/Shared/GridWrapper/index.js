import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import Datatable from '../Datatable'

const GridWrapper = ({
  data,
  columns,
  totalRecords,
  className,
  manual,
  pageSort,
  pagination,
  fetchData,
  ...rest
}) => {
  const { currPage, noOfPages, pageSize } = pagination
  return (
    <Fragment>
      <Datatable
        columns={columns}
        className={className}
        manual={manual}
        pageSort={pageSort}
        totalRecords={totalRecords}
        data={data}
        key={`${noOfPages}${currPage}${pageSize}`}
        page={currPage}
        pages={noOfPages}
        pageSize={pageSize}
        onPageChange={page => fetchData({ page, pageSize })}
        onPageSizeChange={(pgSize, page) =>
          fetchData({ page, pageSize: pgSize })
        }
        extras={rest}
      />
    </Fragment>
  )
}

GridWrapper.propTypes = {
  data: PropTypes.array.isRequired,
  className: PropTypes.string,
  columns: PropTypes.array.isRequired,
  totalRecords: PropTypes.number,
  manual: PropTypes.bool,
  pageSort: PropTypes.bool,
  pagination: PropTypes.object.isRequired,
  fetchData: PropTypes.func.isRequired
}

export default GridWrapper
