import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { Sidebar, Menu, Icon } from 'semantic-ui-react'
import { getRole } from '../../../helpers/HelperMethods'

const renderMenu = (link, history) => {
  const active = path => history.location.pathname === path

  const open = sub =>
    sub.find(item => item.path === history.location.pathname) ? 'open' : ''

  const openSub = e => {
    const parEle = e.target.parentElement
    const similarEle = document.querySelectorAll('.menuItem.open')

    similarEle.forEach(ele => {
      if (ele !== parEle) {
        ele.classList.remove('open')
      }
    })

    if (parEle.classList.contains('open')) {
      parEle.classList.remove('open')
    } else {
      parEle.classList.add('open')
    }
  }

  return (
    <Menu>
      {link.sub && (
        <li className={`menuItem ${open(link.sub)}`}>
          {/* eslint-disable-next-line */}
          <a href="#" className={link.className} onClick={openSub}>
            <Icon name={link.icon} className="pr4" />
            {link.name}
          </a>
          <ul className="subMenu">
            {link.sub.map(
              (subLink, index2) =>
                getRole(subLink.access) && (
                  <li className="menuItem" key={index2}>
                    <NavLink
                      to={subLink.path}
                      isActive={() => active(subLink.path)}
                    >
                      <Icon name={subLink.icon} className="pr4" />
                      {subLink.name}
                    </NavLink>
                  </li>
                )
            )}
          </ul>
        </li>
      )}
      {!link.sub && link.path && (
        <Fragment>
          <NavLink to={link.path} isActive={() => active(link.path)}>
            <Icon name={link.icon} className="pr4" />
            {link.name}
          </NavLink>
        </Fragment>
      )}
    </Menu>
  )
}

const LeftNav = ({ visible, children, history, menu }) => (
  <Sidebar.Pushable as={'div'} style={{ marginTop: '0px' }}>
    <Sidebar as={Menu} animation="push" visible={visible} vertical>
      <div className="navSection">
        {menu.map((link, index) => (
          <Fragment key={index}>
            {getRole(link.access) && renderMenu(link, history)}
          </Fragment>
        ))}
      </div>
    </Sidebar>
    <Sidebar.Pusher>
      <div className="min-vh-100 contentPage">{children}</div>
    </Sidebar.Pusher>
  </Sidebar.Pushable>
)

LeftNav.propTypes = {
  visible: PropTypes.bool.isRequired,
  menu: PropTypes.array.isRequired,
  children: PropTypes.any.isRequired,
  history: PropTypes.any
}

export default LeftNav
