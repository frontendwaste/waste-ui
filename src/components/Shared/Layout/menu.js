export const verticalMenu = [
  {
    name: 'overview',
    className: 'waves-effect',
    icon: 'home',
    path: '/',
    access: ['tenantadmin', 'superadmin']
  },
  {
    name: 'user management',
    className: 'waves-effect',
    icon: 'camera',
    path: '',
    access: ['tenantadmin', 'superadmin'],
    sub: [
      {
        name: 'users',
        className: 'waves-effect',
        path: '/Users',
        access: ['tenantadmin', 'superadmin']
      },
      {
        name: 'roles',
        className: 'waves-effect',
        path: '/Roles',
        access: ['tenantadmin', 'superadmin']
      }
    ]
  },
  {
    name: 'Tenancy',
    className: 'waves-effect',
    icon: 'camera',
    path: '',
    access: ['superadmin'],
    sub: [
      {
        name: 'tenants',
        className: 'waves-effect',
        path: '/Tenants',
        access: ['superadmin']
      }
    ]
  },
  {
    name: 'System Settings',
    className: 'waves-effect',
    icon: 'camera',
    path: '',
    access: ['tenantadmin'],
    sub: [
      {
        name: 'banks',
        className: 'waves-effect',
        path: '/System/Banks',
        access: ['tenantadmin']
      },
      {
        name: 'trucks',
        className: 'waves-effect',
        path: '/System/Trucks',
        access: ['tenantadmin']
      },
      {
        name: 'payment methods',
        className: 'waves-effect',
        path: '/System/PaymentMethods',
        access: ['tenantadmin']
      },
      {
        name: 'countries',
        className: 'waves-effect',
        path: '/System/Country',
        access: ['tenantadmin']
      },
      {
        name: 'states',
        className: 'waves-effect',
        path: '/System/States',
        access: ['tenantadmin']
      },
      {
        name: 'currencies',
        className: 'waves-effect',
        path: '/System/Currency',
        access: ['tenantadmin']
      },
      {
        name: 'zones',
        className: 'waves-effect',
        path: '/System/Zones',
        access: ['tenantadmin']
      },
      {
        name: 'branches',
        className: 'waves-effect',
        path: '/System/Branch',
        access: ['tenantadmin']
      },
      // {
      //   name: 'commission',
      //   className: 'waves-effect',
      //   path: '/System/Commission',
      //  access: ['tenantadmin']
      // },
      {
        name: 'themes',
        className: 'waves-effect',
        path: '/System/Theme',
        access: ['tenantadmin']
      }
    ]
  },
  {
    name: 'Configuration',
    className: 'waves-effect',
    icon: 'camera',
    path: '',
    access: ['tenantadmin'],
    sub: [
      {
        name: 'customer',
        className: 'waves-effect',
        path: '/Config/Customer',
        access: ['tenantadmin']
      },
      {
        name: 'customer type',
        className: 'waves-effect',
        path: '/Config/CustomerType',
        access: ['tenantadmin']
      },
      {
        name: 'Sub-Customer Type',
        className: 'waves-effect',
        path: '/Config/SubCustomerType',
        access: ['tenantadmin']
      },
      {
        name: 'Billing Type',
        className: 'waves-effect',
        path: '/Config/BillingType',
        access: ['tenantadmin']
      }
    ]
  },
  {
    name: 'Payment',
    className: 'waves-effect',
    icon: 'camera',
    path: '',
    access: ['tenantadmin'],
    sub: [
      {
        name: 'transactions',
        className: 'waves-effect',
        path: '/Payment/Transactions',
        access: ['tenantadmin']
      }
    ]
  },
  {
    name: 'Pickup & Billing',
    className: 'waves-effect',
    icon: 'camera',
    path: '',
    access: ['tenantadmin'],
    sub: [
      {
        name: 'pickup',
        className: 'waves-effect',
        path: '/Pickup',
        access: ['tenantadmin']
      },
      {
        name: 'billing',
        className: 'waves-effect',
        path: '/Pickup/Billing',
        access: ['tenantadmin']
      }
    ]
  }
]
