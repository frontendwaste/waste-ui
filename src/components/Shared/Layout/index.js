import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Grid } from 'semantic-ui-react'
import Avatar from 'react-avatar'

import Header from './header'
import LeftNav from './leftNav'
import { verticalMenu } from './menu'
import { getCurrUser } from '../../../helpers/HelperMethods'

class Layout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userElement: null,
      leftNavOpen: true,
      rightNavOpen: false
    }

    this.handleToggle = this.handleToggle.bind(this)
    this.handleLogOut = this.handleLogOut.bind(this)
  }

  componentWillMount() {
    const userObject = getCurrUser()

    if (userObject) {
      this.setState({
        userElement: (
          <span>
            <Avatar name={getCurrUser().name} round size={28} />
            <span className="ph2">{getCurrUser().name}</span>
          </span>
        )
      })
    }
  }

  handleToggle() {
    const { leftNavOpen } = this.state
    this.setState({ leftNavOpen: !leftNavOpen })
  }

  handleLogOut = () => {
    const { history } = this.props

    localStorage.clear()
    history.replace({
      pathname: '/login'
    })
  }

  render() {
    const { history, children } = this.props
    const { userElement, leftNavOpen } = this.state

    return (
      <Fragment>
        <LeftNav visible={leftNavOpen} history={history} menu={verticalMenu}>
          <Grid columns="1">
            <Grid.Row>
              <Grid.Column>
                <Header
                  onToggle={this.handleToggle}
                  user={userElement}
                  logoutAction={this.handleLogOut}
                />
              </Grid.Column>
            </Grid.Row>
            <Grid.Row>
              <Grid.Column>{children}</Grid.Column>
            </Grid.Row>
          </Grid>
        </LeftNav>
      </Fragment>
    )
  }
}

Layout.propTypes = {
  children: PropTypes.any,
  history: PropTypes.any
}

export default Layout
