import React from 'react'
import PropTypes from 'prop-types'

const AccountLayout = ({ children }) => {
  return (
    <div className="flex flex-column w-100 h-100 items-center">
      <img
        src="https://www.freelogodesign.org/Content/img/logo-ex-7.png"
        alt="logo"
        className="w3 mt5 mb3"
      />
      <div className="bg-white login-shadow flex flex-column w-90 w-two-thirds-ns w-40-l pt5 pb5 ph4 ph5-ns ph5-l br2">
        {children}
      </div>
    </div>
  )
}

AccountLayout.propTypes = {
  children: PropTypes.any.isRequired
}

export default AccountLayout
