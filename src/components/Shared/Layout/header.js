import React from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'
import { Menu, Dropdown, Icon } from 'semantic-ui-react'

const Header = ({ onToggle, logoutAction, user }) => {
  return (
    <Menu
      stackable
      secondary
      className="pa2 flex menu secondary stackable ui h3"
      style={{
        border: '1px solid rgba(34,36,38,.15)',
        marginBottom: '0px',
        backgroundColor: 'white'
      }}
    >
      <div className="logoCont flex flex-column flex-wrap justify-center items-center">
        <Menu.Item onClick={onToggle}>
          <Icon name="bars" fitted />
        </Menu.Item>
      </div>
      <Menu.Menu position="right" className="pr3">
        <Dropdown trigger={user} icon="angle down" simple className="icon">
          <Dropdown.Menu>
            <Dropdown.Header>Options</Dropdown.Header>
            <Dropdown.Divider />
            <Dropdown.Item>View Profile</Dropdown.Item>
            <NavLink to="/ChangePassword">Change Password</NavLink>
            <Dropdown.Divider />
            <Menu.Item className="red" onClick={logoutAction} name="logout" />
          </Dropdown.Menu>
        </Dropdown>
      </Menu.Menu>
    </Menu>
  )
}

Header.propTypes = {
  onToggle: PropTypes.func,
  logoutAction: PropTypes.func.isRequired,
  user: PropTypes.any
}

export default Header
