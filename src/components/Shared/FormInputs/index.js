import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Grid } from 'semantic-ui-react'
import { getIn } from 'formik'
import { showError } from '../../../helpers/HelperMethods'

export class FormInputs extends Component {
  constructor(props) {
    super(props)

    this.handleBlur = this.handleBlur.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleBlur() {
    const { form, field } = this.props
    form.setFieldTouched(field.name)
  }

  handleChange(event, { value }) {
    const { form, field } = this.props
    const finalValue = value !== null ? value.trim() : ''
    form.setFieldValue(field.name, finalValue)
  }

  render() {
    const { field, form, label, width, ...rest } = this.props

    return (
      <Grid.Column width={width || 16}>
        <Form.Group widths="equal">
          {rest.type === 'checkbox' && (
            <Form.Field>
              <label htmlFor={field.name}>{label}</label>
              <Form.Checkbox
                {...field}
                {...rest}
                id={rest.id || field.name}
                checked={form.values[field.name]}
              />
            </Form.Field>
          )}

          {rest.type === 'textarea' && (
            <Form.TextArea
              label={label}
              placeholder={label}
              value={field.value}
              {...field}
              {...rest}
              id={rest.id || field.name}
              error={showError(form, field)}
            />
          )}

          {rest.type !== 'checkbox' && rest.type !== 'textarea' && (
            <Form.Input
              fluid
              label={label}
              placeholder={label}
              value={field.value}
              onBlur={this.handleBlur}
              onChange={this.handleChange}
              {...field}
              {...rest}
              id={rest.id || field.name}
              error={showError(form, field)}
            />
          )}
          {showError(form, field) && (
            <small className="red ph2">
              {form.errors[field.name] || getIn(form.errors, field.name)}
            </small>
          )}
        </Form.Group>
      </Grid.Column>
    )
  }
}

FormInputs.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired
}

export default FormInputs
