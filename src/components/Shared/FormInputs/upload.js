import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Form, Grid } from 'semantic-ui-react'
import Dropzone from 'react-dropzone'
import { showError } from '../../../helpers/HelperMethods'

export class FormUploadInput extends Component {
  constructor(props) {
    super(props)

    this.onDrop = this.onDrop.bind(this)
  }

  onDrop(acceptedFiles) {
    const { form, field } = this.props

    acceptedFiles.forEach(file => {
      const reader = new FileReader() // eslint-disable-line

      reader.onload = () => {
        const dataUrl = reader.result
        form.setFieldValue(field.name, dataUrl)
      }

      reader.readAsBinaryString(file)
    })
  }

  render() {
    const { field, form, label, width, ...rest } = this.props

    return (
      <Grid.Column width={width || 16}>
        <Form.Group widths="equal">
          <Form.Field>
            {label && (
              <label
                htmlFor={field.name}
                className={`${rest.required && 'required'}`}
              >
                {label}
              </label>
            )}
            <Dropzone
              accept="text/csv"
              onDrop={this.onDrop}
              {...rest}
              className="flex flex-row justify-center items-center br0 w-100 h5 b--dotted bw1 b--light-silver"
              style={{ backgroundColor: '#f7f7f7' }}
              activeClassName="bg-light-green"
              rejectClassName="bg-light-red"
            >
              {({ acceptedFiles, rejectedFiles }) => {
                if (acceptedFiles.length && acceptedFiles.length > 0) {
                  return `${acceptedFiles[0].name}`
                }
                if (rejectedFiles.length && rejectedFiles.length > 0) {
                  return 'File format not supported (supports excel and csv only)'
                }
                return 'Drag and drop your file here (supports excel and csv)'
              }}
            </Dropzone>
            {showError(form, field) && (
              <small className="red ph2">{form.errors[field.name]}</small>
            )}
          </Form.Field>
        </Form.Group>
      </Grid.Column>
    )
  }
}

FormUploadInput.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  label: PropTypes.string
}

export default FormUploadInput
