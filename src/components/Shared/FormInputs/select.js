import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Form, Grid } from 'semantic-ui-react'
import { showError } from '../../../helpers/HelperMethods'

export class FormSelect extends Component {
  constructor(props) {
    super(props)

    this.handleBlur = this.handleBlur.bind(this)
    this.handleChange = this.handleChange.bind(this)
  }

  handleBlur() {
    const { form, field } = this.props
    form.setFieldTouched(field.name)
  }

  handleChange(event, { value }) {
    const { form, field } = this.props
    form.setFieldValue(field.name, value)
  }

  render() {
    const { field, form, label, options, required, width, ...rest } = this.props

    const selectOptions = options.map(option => ({
      id: option.id,
      value: option.id,
      text: option.name
    }))

    return (
      <Grid.Column width={width || 16}>
        <Form.Group widths="equal">
          <Form.Field error={showError(form, field)}>
            <label htmlFor={field.name} className={`${required && 'required'}`}>
              {label}
            </label>
            <Form.Select
              {...rest}
              options={selectOptions}
              value={field.value}
              onBlur={this.handleBlur}
              onChange={this.handleChange}
            />
            {showError(form, field) && (
              <small className="red">{form.errors[field.name]}</small>
            )}
          </Form.Field>
        </Form.Group>
      </Grid.Column>
    )
  }
}

FormSelect.propTypes = {
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  required: PropTypes.bool,
  label: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired
}

export default FormSelect
