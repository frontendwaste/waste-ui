import React from 'react'
import PropTypes from 'prop-types'
import { Form, Grid } from 'semantic-ui-react'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import { showError } from '../../../helpers/HelperMethods'

export const DatePickerInput = props => {
  const { field, form, label, required, width, ...rest } = props

  return (
    <Grid.Column width={width || 16}>
      <Form.Group widths="equal">
        <Form.Field>
          <label
            htmlFor={rest.id || field.name}
            className={required ? 'required' : ''}
          >
            {label}
          </label>
          <DatePicker
            className="form-control"
            {...field}
            {...rest}
            id={rest.id || field.name}
            onChange={value =>
              form.setFieldValue(field.name, value.format('YYYY-MM-DD HH:mm'))
            }
            onBlur={() => null}
            selected={moment(field.value)}
            minDate={new Date()}
            dateFormat="YYYY-MM-DD"
            timeFormat="HH:mm"
            showTimeSelect
            timeIntervals={30}
            placeholderText={label}
          />
        </Form.Field>
        {showError(form, field) && (
          <small className="red ph2">{form.errors[field.name]}</small>
        )}
      </Form.Group>
    </Grid.Column>
  )
}

DatePickerInput.propTypes = {
  required: PropTypes.bool.isRequired,
  field: PropTypes.object.isRequired,
  form: PropTypes.object.isRequired,
  label: PropTypes.string.isRequired
}

export default DatePickerInput
