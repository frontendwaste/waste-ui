import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Grid, Icon } from 'semantic-ui-react'
import { Form, Field, FieldArray } from 'formik'
import { validateAll } from '../../../helpers/HelperMethods'

const renderFieldsArray = ({
  name,
  fields,
  generateMemberField,
  removeMemberField
}) => (
  <Grid.Row>
    <FieldArray
      name={name}
      render={({ remove }) => (
        <Fragment>
          {generateMemberField && (
            <Grid.Column width={16}>
              <div className="flex flex-column flex-row-ns flex-wrap items-center mb3">
                <hr className="w-100 b--black-025 mv3" />
                <button
                  type="button"
                  className="pointer underline-hover b--none bg-transparent pb3"
                  onClick={() => generateMemberField()}
                >
                  Add Your RSVP&nbsp;
                  <Icon name="add" size="small" />
                </button>
              </div>
            </Grid.Column>
          )}
          {fields.length > 0 &&
            fields.map((field, index) => (
              <Fragment key={index}>
                {Object.keys(field).map((item, index2) => (
                  <Field
                    key={index2}
                    {...field[item]}
                    name={`${name}[${index}].${field[item].name}`}
                  />
                ))}
                {removeMemberField && (
                  <Grid.Column width={1}>
                    <div className="flex flex-column flex-row-ns mt4-l">
                      <Icon
                        size="large"
                        color="red"
                        name="cancel"
                        link
                        onClick={() => {
                          removeMemberField(index)
                          remove(index)
                        }}
                      />
                    </div>
                  </Grid.Column>
                )}
              </Fragment>
            ))}
        </Fragment>
      )}
    />
  </Grid.Row>
)

const FormWrapper = ({ fields, fieldsArray, className, button }) => (
  <Form className={`ui form ${className}`}>
    <Grid stackable padded="vertically">
      <Grid.Row>
        {fields.map(({ validate, ...rest }, index) => (
          <Field {...rest} validate={validate || validateAll} key={index} />
        ))}
      </Grid.Row>
      {fieldsArray && renderFieldsArray(fieldsArray)}
    </Grid>
    <div className="flex flex-row">
      {button &&
        button.length > 0 &&
        button.map(({ title, ...rest }, index) => (
          <Fragment key={index}>
            <Button size="small" {...rest}>
              {title}
            </Button>
            &nbsp;
          </Fragment>
        ))}
    </div>
  </Form>
)

FormWrapper.propTypes = {
  fields: PropTypes.array.isRequired,
  button: PropTypes.array.isRequired,
  fieldsArray: PropTypes.object,
  className: PropTypes.string
}

export default FormWrapper
