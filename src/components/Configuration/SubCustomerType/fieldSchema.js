import FormInputs from '../../Shared/FormInputs'
import FormSelect from '../../Shared/FormInputs/select'

export const formFields = ({ customerTypeData, billingTypeData }) => {
  return [
    {
      type: 'text',
      name: 'name',
      label: 'Name',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'number',
      name: 'baseAmount',
      label: 'Base Amount',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      name: 'customerTypeId',
      label: 'Customer Type',
      options: customerTypeData.data || [],
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      name: 'billingTypeId',
      label: 'Billing Type',
      options: billingTypeData.data || [],
      component: FormSelect,
      required: true,
      width: 4
    }
  ]
}
