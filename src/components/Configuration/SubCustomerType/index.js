import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Modal } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import SubCustomerTypeGrid from './subCustomerTypeGrid'
import { formFields } from './fieldSchema'

const SubCustomerType = props => {
  const {
      history,
      modalVisible,
      modalActionName,
      selectedSubCustomerType,
      formSubmit,
      modalToggle
    } = props,
    initialValues =
      modalActionName === 'edit'
        ? { initialValues: selectedSubCustomerType }
        : {}

  return (
    <Fragment>
      <Layout history={history}>
        <Card fluid>
          <Card.Content>
            <Card.Header>Existing Sub-Customer Types</Card.Header>
            <Button
              basic
              compact
              floated="right"
              onClick={() => modalToggle('create', modalVisible)}
            >
              Create
            </Button>
          </Card.Content>
          <Card.Content>
            <SubCustomerTypeGrid {...props} />
          </Card.Content>
        </Card>
      </Layout>

      <Modal
        onClose={() => modalToggle('', modalVisible)}
        open={modalVisible}
        size="small"
        closeIcon
      >
        <Modal.Header>{modalActionName} Sub-Customer Type</Modal.Header>
        <Modal.Content>
          <Formik
            {...initialValues}
            validationSchema={fieldValidation(formFields(props))}
            onSubmit={formSubmit}
            component={() => (
              <FormWrapper
                fields={formFields(props)}
                button={[
                  {
                    title: modalActionName,
                    className: 'bg-primary-color',
                    type: 'submit'
                  }
                ]}
              />
            )}
          />
        </Modal.Content>
      </Modal>
    </Fragment>
  )
}

SubCustomerType.propTypes = {
  history: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  modalActionName: PropTypes.string.isRequired,
  subCustomerTypeData: PropTypes.object.isRequired,
  customerTypeData: PropTypes.object.isRequired,
  billingTypeData: PropTypes.object.isRequired,
  selectedSubCustomerType: PropTypes.object.isRequired,
  fetchGridData: PropTypes.func.isRequired,
  formSubmit: PropTypes.func.isRequired,
  modalToggle: PropTypes.func.isRequired,
  actionClick: PropTypes.func.isRequired
}

export default SubCustomerType
