import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import GridWrapper from '../../Shared/GridWrapper'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Edit Sub-Customer Type"
        onClick={() => actionClick(row.id, 'edit', data, modalVisible)}
      >
        Edit
      </Button>
    </Fragment>
  )
}

const SubCustomerTypeGrid = ({
  subCustomerTypeData,
  actionClick,
  modalVisible,
  fetchGridData,
  pagination
}) => {
  return (
    <GridWrapper
      columns={[
        {
          Header: 'Name',
          accessor: 'name'
        },
        {
          Header: 'Customer Type',
          accessor: 'customerType.name'
        },
        {
          Header: 'Base Amount',
          accessor: 'baseAmount'
        },
        {
          Header: 'Billing Type',
          accessor: 'billingType.name'
        },
        {
          Header: 'Option',
          accessor: 'id',
          Cell: cellProps =>
            composeCell(cellProps, {
              actionClick,
              data: subCustomerTypeData.data,
              modalVisible
            })
        }
      ]}
      data={subCustomerTypeData.data || []}
      pagination={pagination}
      fetchData={fetchGridData}
    />
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

SubCustomerTypeGrid.propTypes = {
  subCustomerTypeData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired,
  fetchGridData: PropTypes.func.isRequired
}

export default SubCustomerTypeGrid
