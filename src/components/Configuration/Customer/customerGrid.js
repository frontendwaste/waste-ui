import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Dropdown } from 'semantic-ui-react'

import GridWrapper from '../../Shared/GridWrapper'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  const options = [
    {
      key: 'edit',
      icon: 'edit',
      text: 'Edit',
      onClick: () => actionClick(row.id, 'edit', data, modalVisible)
    },
    {
      key: 'wallet',
      icon: 'credit card',
      text: 'Wallet',
      onClick: () => actionClick(row.id, 'wallet', data, modalVisible)
    }
  ]

  return (
    <Fragment>
      <Dropdown
        trigger={<i />}
        options={options}
        direction="left"
        floating
        button
        compact
        icon="ellipsis horizontal"
        className="bg-primary-color no-child-margin"
      />
    </Fragment>
  )
}

const CustomerGrid = ({
  customerData,
  actionClick,
  modalVisible,
  fetchGridData,
  zoneData,
  pagination
}) => {
  return (
    <GridWrapper
      columns={
        (!!zoneData.records &&
          !!zoneData.records.length && [
          {
            Header: 'Customer Code',
            accessor: 'code'
          },
          {
            Header: 'First Name',
            accessor: 'firstName'
          },
          {
            Header: 'Last Name',
            accessor: 'lastName'
          },
          {
            Header: 'Customer Type',
            accessor: 'customerType.name'
          },
          {
            Header: 'Base Amount',
            accessor: 'baseAmount'
          },
          {
            Header: 'Zone',
            accessor: 'zoneId',
            Cell: ({ value }) => {
              const zone = zoneData.records.find(zone => zone.id === value)
              return (!!zone && zone.name) || ''
            }
          },
          {
            Header: 'Tagged',
            accessor: 'isTagged',
            Cell: ({ value }) => (value === '1' ? 'Yes' : 'No')
          },
          {
            Header: 'Option',
            accessor: 'id',
            Cell: cellProps =>
              composeCell(cellProps, {
                actionClick,
                data: customerData.data.rows,
                modalVisible
              })
          }
        ]) ||
        []
      }
      data={(customerData.data && customerData.data.rows) || []}
      totalRecords={parseInt(
        customerData.data && customerData.data.totalRecords,
        10
      )}
      pagination={pagination}
      fetchData={fetchGridData}
    />
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

CustomerGrid.propTypes = {
  customerData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired,
  fetchGridData: PropTypes.func.isRequired,
  zoneData: PropTypes.object.isRequired
}

export default CustomerGrid
