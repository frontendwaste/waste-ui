import FormInputs from '../../Shared/FormInputs'
import FormSelect from '../../Shared/FormInputs/select'
import DatePicker from '../../Shared/FormInputs/datepicker'

export const formFields = ({ customerTypeData, zoneData, branchData }) => {
  return [
    {
      type: 'text',
      name: 'firstName',
      label: 'First Name',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      name: 'lastName',
      label: 'Last Name',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'email',
      name: 'email',
      label: 'Email',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      pattern: '[0-9]*',
      name: 'phoneNumber',
      label: 'Phone Number',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'number',
      name: 'baseAmount',
      label: 'Base Amount',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      type: 'text',
      name: 'address',
      label: 'Address',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      name: 'customerTypeId',
      label: 'Customer Type',
      options: customerTypeData.data || [],
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      name: 'zoneId',
      label: 'Zone',
      options: zoneData.records || [],
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      name: 'branchId',
      label: 'Branch',
      options: branchData.records || [],
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      type: 'checkbox',
      name: 'isTagged',
      label: 'Tag',
      component: FormInputs,
      required: false,
      width: 4
    }
  ]
}

export const searchFormFields = ({ customerTypeData, zoneData }) => {
  return [
    {
      name: 'startDate',
      label: 'Start Date',
      component: DatePicker,
      required: false,
      width: 4
    },
    {
      name: 'endDate',
      label: 'End Date',
      component: DatePicker,
      required: false,
      width: 4
    },
    {
      name: 'zoneId',
      label: 'Zone',
      options: zoneData.records || [],
      component: FormSelect,
      required: false,
      width: 4
    },
    {
      name: 'customerTypeId',
      label: 'Customer Type',
      options: customerTypeData.data || [],
      component: FormSelect,
      required: false,
      width: 4
    },
    {
      name: 'isTagged',
      label: 'Tagged',
      options: [{ name: 'Yes', id: 1 }, { name: 'No', id: 0 }],
      component: FormSelect,
      required: false,
      width: 4
    }
  ]
}

export const uploadFormFields = (
  { customerTypeData, zoneData, branchData, uploadingData },
  setFieldValue
) => [
  {
    name: 'branchId',
    label: 'Branch',
    options: branchData.records || [],
    component: FormSelect,
    required: true,
    width: 4
  },
  {
    name: 'customerTypeId',
    label: 'Customer Type',
    options: customerTypeData.data || [],
    component: FormSelect,
    required: true,
    width: 4
  },
  {
    name: 'zoneId',
    label: 'Zone',
    options: zoneData.records || [],
    component: FormSelect,
    required: true,
    width: 4
  },
  {
    type: 'file',
    name: 'customers',
    label: 'Customer Upload',
    component: FormInputs,
    onChange: event => {
      const file = event.currentTarget.files[0]
      setFieldValue('file', file)
      uploadingData(file)
    },
    required: false,
    multiple: false,
    width: 4
  }
]
