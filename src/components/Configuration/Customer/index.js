import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Modal } from 'semantic-ui-react'
import moment from 'moment'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import CustomerGrid from './customerGrid'
import { formFields, searchFormFields, uploadFormFields } from './fieldSchema'

const renderWallet = walletData => {
  console.log(walletData)
  return 'wallet'
}

const Customer = props => {
  const {
      history,
      modalVisible,
      modalActionName,
      selectedCustomer,
      customerWalletData,
      uploadData,
      downloadTemplate,
      formSubmit,
      uploadFormSubmit,
      searchFormSubmit,
      modalToggle
    } = props,
    initialValues =
      (modalActionName === 'edit' && {
        initialValues: {
          ...selectedCustomer,
          isTagged: selectedCustomer.isTagged === '1' ? true : false
        }
      }) ||
      {}

  return (
    <Fragment>
      <Layout history={history}>
        <Card fluid>
          <Card.Content>
            <Card.Header>Existing Customers</Card.Header>
            <Button
              basic
              compact
              floated="right"
              onClick={() => modalToggle('create', modalVisible)}
            >
              Create
            </Button>
            <Button
              basic
              compact
              floated="right"
              onClick={() => modalToggle('upload', modalVisible)}
            >
              Upload
            </Button>
            <Button
              basic
              compact
              floated="right"
              onClick={() => downloadTemplate()}
            >
              Download Template
            </Button>
          </Card.Content>
          <Card.Content>
            <Formik
              initialValues={{
                startDate: moment().format('YYYY-MM-DD'),
                endDate: moment().format('YYYY-MM-DD')
              }}
              validationSchema={fieldValidation(searchFormFields(props))}
              onSubmit={searchFormSubmit}
              component={() => (
                <FormWrapper
                  fields={searchFormFields(props)}
                  button={[
                    {
                      title: 'Search',
                      className: 'bg-primary-color',
                      type: 'submit'
                    }
                  ]}
                />
              )}
            />
          </Card.Content>
          <Card.Content>
            <CustomerGrid {...props} />
          </Card.Content>
        </Card>
      </Layout>

      <Modal
        onClose={() => modalToggle('', modalVisible)}
        open={modalVisible}
        size="large"
        closeIcon
      >
        {modalActionName && (
          <Modal.Header>{modalActionName} Customer</Modal.Header>
        )}
        <Modal.Content>
          {(modalActionName === 'create' || modalActionName === 'edit') && (
            <Formik
              {...initialValues}
              validationSchema={fieldValidation(formFields(props))}
              onSubmit={formSubmit}
              component={() => (
                <FormWrapper
                  fields={formFields(props)}
                  button={[
                    {
                      title: modalActionName,
                      className: 'bg-primary-color',
                      type: 'submit'
                    }
                  ]}
                />
              )}
            />
          )}
          {modalActionName === 'upload' && (
            <Formik
              initialValues={{}}
              validationSchema={fieldValidation(uploadFormFields(props))}
              onSubmit={(values, actions) =>
                uploadFormSubmit(values, actions, uploadData)
              }
              render={({ setFieldValue }) => (
                <FormWrapper
                  fields={uploadFormFields(props, setFieldValue)}
                  button={[
                    {
                      title: modalActionName,
                      className: 'bg-primary-color',
                      type: 'submit'
                    }
                  ]}
                />
              )}
            />
          )}
          {modalActionName === 'wallet' && renderWallet(customerWalletData)}
        </Modal.Content>
      </Modal>
    </Fragment>
  )
}

Customer.propTypes = {
  history: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  modalActionName: PropTypes.string.isRequired,
  uploadData: PropTypes.object.isRequired,
  selectedCustomer: PropTypes.object.isRequired,
  customerWalletData: PropTypes.object.isRequired,
  downloadTemplate: PropTypes.func.isRequired,
  formSubmit: PropTypes.func.isRequired,
  uploadFormSubmit: PropTypes.func.isRequired,
  searchFormSubmit: PropTypes.func.isRequired,
  modalToggle: PropTypes.func.isRequired
}

export default Customer
