import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import GridWrapper from '../../Shared/GridWrapper'

const composeCell = ({ row }, { voidTransaction, validateTransaction }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Validate Transaction"
        onClick={() => validateTransaction(row.id)}
      >
        Validate
      </Button>
      &nbsp;
      <Button
        size="mini"
        compact
        title="Void Transaction"
        onClick={() => voidTransaction(row.id)}
      >
        Void
      </Button>
    </Fragment>
  )
}

const modCellHtml = ({ value }) => {
  const labelClass = `label ${(value === 'Approved' && 'label-success') ||
    (value === 'Pending' && 'label-warning') ||
    'label-danger'}`

  return <span className={labelClass}>{value}</span>
}

const TransactionGrid = ({
  transactionData,
  voidTransaction,
  validateTransaction,
  fetchGridData,
  channelData,
  userData,
  pagination
}) => {
  return (
    !!userData.data &&
    !!userData.data.Records &&
    !!userData.data.Records.length &&
    !!channelData.data &&
    !!channelData.data.length && (
      <GridWrapper
        columns={
          [
            {
              Header: 'Channel Type',
              accessor: 'channelId',
              Cell: ({ value }) => {
                const channel = channelData.data.find(type => type.id === value)
                return (!!channel && channel.name) || ''
              }
            },
            {
              Header: 'Amount',
              accessor: 'amount'
            },
            {
              Header: 'User',
              accessor: 'customerCode',
              Cell: ({ value }) => {
                const user = userData.data.Records.find(
                  user => user.id === value
                )
                return (!!user && `${user.firstName} ${user.lastName}`) || ''
              }
            },
            {
              Header: 'Status',
              accessor: 'status.name',
              Cell: modCellHtml
            },
            {
              Header: 'Option',
              accessor: 'id',
              Cell: cellProps =>
                composeCell(cellProps, {
                  voidTransaction,
                  validateTransaction
                })
            }
          ] || []
        }
        data={
          (transactionData &&
            transactionData.data &&
            transactionData.data.rows) ||
          []
        }
        totalRecords={parseInt(
          transactionData.data && transactionData.data.totalRecords,
          10
        )}
        pagination={pagination}
        fetchData={fetchGridData}
      />
    )
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

modCellHtml.propTypes = {
  value: PropTypes.string.isRequired
}

TransactionGrid.propTypes = {
  transactionData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  voidTransaction: PropTypes.func.isRequired,
  validateTransaction: PropTypes.func.isRequired,
  fetchGridData: PropTypes.func.isRequired,
  userData: PropTypes.object.isRequired
}

export default TransactionGrid
