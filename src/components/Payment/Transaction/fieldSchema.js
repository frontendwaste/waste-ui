import FormInputs from '../../Shared/FormInputs'
import FormSelect from '../../Shared/FormInputs/select'
import DatePicker from '../../Shared/FormInputs/datepicker'

export const formFields = ({ userData, channelData }) => {
  return [
    {
      type: 'number',
      name: 'amount',
      label: 'Amount',
      component: FormInputs,
      required: true,
      width: 4
    },
    {
      name: 'userId',
      label: 'User',
      options:
        (userData.data &&
          userData.data.Records.map(user => ({
            ...user,
            name: `${user.firstName}  ${user.lastName}`
          }))) ||
        [],
      component: FormSelect,
      required: false,
      width: 4
    },
    {
      name: 'channelId',
      label: 'Channel',
      options: channelData.data || [],
      component: FormSelect,
      required: false,
      width: 4
    }
  ]
}

export const searchFormFields = ({ userData, statusData, channelData }) => {
  return [
    {
      name: 'startDate',
      label: 'Start Date',
      component: DatePicker,
      required: false,
      width: 4
    },
    {
      name: 'endDate',
      label: 'End Date',
      component: DatePicker,
      required: false,
      width: 4
    },
    {
      type: 'text',
      name: 'channelName',
      label: 'Channel Name',
      component: FormInputs,
      required: false,
      width: 4
    },
    {
      name: 'userId',
      label: 'User',
      options:
        (userData.data &&
          userData.data.Records.map(user => ({
            ...user,
            name: `${user.firstName}  ${user.lastName}`
          }))) ||
        [],
      component: FormSelect,
      required: false,
      width: 4
    },
    {
      name: 'channelId',
      label: 'Channel',
      options: channelData.data || [],
      component: FormSelect,
      required: false,
      width: 4
    },
    {
      name: 'statusId',
      label: 'Status',
      options: statusData.data || [],
      component: FormSelect,
      required: false,
      width: 4
    }
  ]
}
