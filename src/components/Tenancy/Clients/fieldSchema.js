import FormInputs from '../../Shared/FormInputs'

export const formFields = [
  {
    type: 'text',
    name: 'name',
    label: 'Tenant Name',
    component: FormInputs,
    required: true
  },
  {
    type: 'text',
    name: 'code',
    label: 'Tenant Code',
    component: FormInputs,
    required: true
  },
  {
    type: 'email',
    name: 'email',
    label: 'Email',
    component: FormInputs,
    required: true
  },
  {
    type: 'text',
    name: 'desc',
    label: 'Description',
    component: FormInputs,
    required: true
  },
  {
    type: 'text',
    name: 'address',
    label: 'Address',
    component: FormInputs,
    required: true
  }
]
