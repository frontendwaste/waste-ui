import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'
import ReactTable from 'react-table'

import 'react-table/react-table.css'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Edit Client"
        onClick={() => actionClick(row.id, 'edit', data, modalVisible)}
      >
        Edit
      </Button>
    </Fragment>
  )
}

const ClientGrid = props => {
  return (
    <Fragment>
      <ReactTable
        columns={[
          {
            Header: 'Creation Date',
            accessor: 'creationDate'
          },
          {
            Header: 'Tenant Name',
            accessor: 'name'
          },
          {
            Header: 'Tenant Code',
            accessor: 'code'
          },
          {
            Header: 'Email',
            accessor: 'email'
          },
          {
            Header: 'Option',
            accessor: 'id',
            Cell: cellProps => composeCell(cellProps, props)
          }
        ]}
        data={props.data}
        defaultPageSize={10}
        className="gilroymedium"
      />
    </Fragment>
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

ClientGrid.propTypes = {
  data: PropTypes.array.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired
}

export default ClientGrid
