import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Modal } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import ClientGrid from './clientGrid'
import { formFields } from './fieldSchema'

const Client = ({
  history,
  modalVisible,
  modalActionName,
  clientsData,
  selectedClient,
  formSubmit,
  actionClick,
  modalToggle
}) => {
  const initialValues =
    modalActionName === 'edit' ? { initialValues: selectedClient } : {}

  return (
    <Fragment>
      <Layout history={history}>
        {clientsData.records && (
          <Card fluid>
            <Card.Content>
              <Card.Header>Existing Clients</Card.Header>
              <Button
                basic
                compact
                floated="right"
                onClick={() => modalToggle('create', modalVisible)}
              >
                Create
              </Button>
            </Card.Content>
            <Card.Content>
              <ClientGrid
                data={clientsData.records || []}
                actionClick={actionClick}
                modalVisible={modalVisible}
              />
            </Card.Content>
          </Card>
        )}
      </Layout>

      <Modal
        onClose={() => modalToggle('', modalVisible)}
        open={modalVisible}
        size="small"
        closeIcon
      >
        <Modal.Header>{modalActionName} Client</Modal.Header>
        <Modal.Content>
          <Formik
            {...initialValues}
            validationSchema={fieldValidation(formFields)}
            onSubmit={formSubmit}
            component={() => (
              <FormWrapper fields={formFields} action={modalActionName} />
            )}
          />
        </Modal.Content>
      </Modal>
    </Fragment>
  )
}

Client.propTypes = {
  history: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  modalActionName: PropTypes.string.isRequired,
  clientsData: PropTypes.object.isRequired,
  selectedClient: PropTypes.object.isRequired,
  formSubmit: PropTypes.func.isRequired,
  modalToggle: PropTypes.func.isRequired,
  actionClick: PropTypes.func.isRequired
}

export default Client
