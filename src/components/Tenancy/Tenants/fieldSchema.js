import FormInputs from '../../Shared/FormInputs'

export const formFields = [
  {
    type: 'text',
    name: 'name',
    label: 'Tenant Name',
    component: FormInputs,
    required: true
  },
  {
    type: 'email',
    name: 'email',
    label: 'Email',
    component: FormInputs,
    required: true
  },
  {
    type: 'text',
    name: 'description',
    label: 'Description',
    component: FormInputs,
    required: true
  },
  {
    type: 'text',
    name: 'weburl',
    label: 'Web URL',
    component: FormInputs,
    required: true
  },
  {
    type: 'text',
    name: 'address',
    label: 'Address',
    component: FormInputs,
    required: true
  },
  {
    type: 'checkbox',
    name: 'status',
    label: 'Enabled',
    component: FormInputs,
    required: false
  }
]
