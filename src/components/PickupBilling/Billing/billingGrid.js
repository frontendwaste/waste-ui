import React from 'react'
import PropTypes from 'prop-types'

import GridWrapper from '../../Shared/GridWrapper'

const modCellHtml = ({ value }) => {
  const labelClass = `label ${(value === 'Approved' && 'label-success') ||
    (value === 'Pending' && 'label-warning') ||
    'label-danger'}`

  return <span className={labelClass}>{value}</span>
}

const BillingGrid = ({ billingData, fetchGridData, pagination }) => {
  return (
    <GridWrapper
      columns={[
        {
          Header: 'Available Balance',
          accessor: 'availableBalance'
        },
        {
          Header: 'Book Balance',
          accessor: 'bookBalance'
        },
        {
          Header: 'Status',
          accessor: 'statusId'
        }
      ]}
      data={(billingData && billingData.data && billingData.data.rows) || []}
      totalRecords={parseInt(
        billingData && billingData.data && billingData.data.count,
        10
      )}
      pagination={pagination}
      fetchData={fetchGridData}
    />
  )
}

modCellHtml.propTypes = {
  value: PropTypes.string.isRequired
}

BillingGrid.propTypes = {
  billingData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  fetchGridData: PropTypes.func.isRequired
}

export default BillingGrid
