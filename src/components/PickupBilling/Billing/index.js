import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Card } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import BillingGrid from './billingGrid'
import { searchFormFields } from './fieldSchema'

const Billing = props => {
  const { history, searchFormSubmit } = props

  return (
    <Fragment>
      <Layout history={history}>
        <Card fluid>
          <Card.Content>
            <Card.Header>Existing Billing</Card.Header>
          </Card.Content>
          <Card.Content>
            <Formik
              initialValues={{}}
              validationSchema={fieldValidation(searchFormFields(props))}
              onSubmit={searchFormSubmit}
              component={() => (
                <FormWrapper
                  fields={searchFormFields(props)}
                  button={[
                    {
                      title: 'Search',
                      className: 'bg-primary-color',
                      type: 'submit'
                    }
                  ]}
                />
              )}
            />
          </Card.Content>
          <Card.Content>
            <BillingGrid {...props} />
          </Card.Content>
        </Card>
      </Layout>
    </Fragment>
  )
}

Billing.propTypes = {
  history: PropTypes.object.isRequired,
  searchFormSubmit: PropTypes.func.isRequired
}

export default Billing
