import FormSelect from '../../Shared/FormInputs/select'
import DatePicker from '../../Shared/FormInputs/datepicker'

export const formFields = ({
  customerData,
  zoneData,
  userData,
  statusData
}) => {
  return [
    {
      name: 'pickupDate',
      label: 'Pickup Date',
      component: DatePicker,
      required: true,
      width: 4
    },
    {
      name: 'customerCode',
      label: 'Customer',
      options:
        (!!customerData.data &&
          customerData.data.length &&
          customerData.data.map(cust => ({
            id: cust.code,
            name: `${cust.firstName} ${cust.lastName}`
          }))) ||
        [],
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      name: 'userId',
      label: 'User',
      options:
        (userData.data &&
          userData.data.Records &&
          userData.data.Records.map(user => ({
            ...user,
            name: `${user.firstName} ${user.lastName}`
          }))) ||
        [],
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      name: 'zoneId',
      label: 'Zone',
      options: zoneData.records || [],
      component: FormSelect,
      required: true,
      width: 4
    },
    {
      name: 'statusId',
      label: 'Status',
      options: statusData.data || [],
      component: FormSelect,
      required: true,
      width: 4
    }
  ]
}

export const searchFormFields = ({
  customerData,
  zoneData,
  statusData,
  userData,
  billingTypeData
}) => {
  return [
    {
      name: 'startDate',
      label: 'Start Date',
      component: DatePicker,
      required: false,
      width: 4
    },
    {
      name: 'endDate',
      label: 'End Date',
      component: DatePicker,
      required: false,
      width: 4
    },
    {
      name: 'pickupDate',
      label: 'Pickup Date',
      component: DatePicker,
      required: false,
      width: 4
    },
    {
      name: 'customerCode',
      label: 'Customer',
      options:
        (!!customerData.data &&
          customerData.data.length &&
          customerData.data.map(cust => ({
            id: cust.code,
            name: `${cust.firstName} ${cust.lastName}`
          }))) ||
        [],
      component: FormSelect,
      required: false,
      width: 4
    },
    {
      name: 'userId',
      label: 'User',
      options:
        (userData.data &&
          userData.data.Records &&
          userData.data.Records.map(user => ({
            ...user,
            name: `${user.firstName} ${user.lastName}`
          }))) ||
        [],
      component: FormSelect,
      required: false,
      width: 4
    },
    {
      name: 'zoneId',
      label: 'Zone',
      options: zoneData.records || [],
      component: FormSelect,
      required: false,
      width: 4
    },
    {
      name: 'statusId',
      label: 'Status',
      options: statusData.data || [],
      component: FormSelect,
      required: false,
      width: 4
    },
    {
      name: 'billingTypeId',
      label: 'Billing Type',
      options: billingTypeData.data || [],
      component: FormSelect,
      required: false,
      width: 4
    }
  ]
}
