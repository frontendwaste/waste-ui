import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button } from 'semantic-ui-react'

import GridWrapper from '../../Shared/GridWrapper'

const composeCell = ({ row }, { actionClick, data, modalVisible }) => {
  return (
    <Fragment>
      <Button
        size="mini"
        compact
        title="Edit Bank"
        onClick={() => actionClick(row.id, 'edit', data, modalVisible)}
      >
        Edit
      </Button>
    </Fragment>
  )
}

const modCellHtml = ({ value }) => {
  const labelClass = `label ${(value === 'Active' && 'label-success') ||
    (value === 'Pending' && 'label-warning') ||
    (value === 'Inactive' && 'label-danger') ||
    'label-info'}`

  return <span className={labelClass}>{value}</span>
}

const PickupGrid = ({
  pickupData,
  customerData,
  zoneData,
  billingTypeData,
  actionClick,
  modalVisible,
  fetchGridData,
  pagination
}) => {
  return (
    <GridWrapper
      columns={
        (!!billingTypeData.data &&
          !!billingTypeData.data.length &&
          !!customerData.data &&
          !!customerData.data.length &&
          !!zoneData.records &&
          !!zoneData.records.length && [
          {
            Header: 'Billing Type',
            accessor: 'billingTypeId',
            Cell: ({ value }) => {
              const billType = billingTypeData.data.find(
                type => type.id === value
              )
              return (!!billType && billType.name) || ''
            }
          },
          {
            Header: 'Pickup Date',
            accessor: 'pickupDate'
          },
          {
            Header: 'Customer',
            accessor: 'customerCode',
            Cell: ({ value }) => {
              const cust = customerData.data.find(cust => cust.code === value)
              return (!!cust && `${cust.firstName} ${cust.lastName}`) || ''
            }
          },
          {
            Header: 'Zone',
            accessor: 'zoneId',
            Cell: ({ value }) => {
              const zone = zoneData.records.find(zone => zone.id === value)
              return (!!zone && `${zone.name}`) || ''
            }
          },
          {
            Header: 'Status',
            accessor: 'status.name',
            Cell: modCellHtml
          },
          {
            Header: 'Option',
            accessor: 'id',
            Cell: cellProps =>
              composeCell(cellProps, {
                actionClick,
                data: pickupData.data.rows,
                modalVisible
              })
          }
        ]) ||
        []
      }
      data={(pickupData && pickupData.data && pickupData.data.rows) || []}
      totalRecords={parseInt(
        pickupData && pickupData.data && pickupData.data.totalRecords,
        10
      )}
      pagination={pagination}
      fetchData={fetchGridData}
    />
  )
}

composeCell.propTypes = {
  row: PropTypes.object.isRequired
}

modCellHtml.propTypes = {
  value: PropTypes.string.isRequired
}

PickupGrid.propTypes = {
  pickupData: PropTypes.object.isRequired,
  pagination: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  actionClick: PropTypes.func.isRequired,
  fetchGridData: PropTypes.func.isRequired,
  customerData: PropTypes.object.isRequired,
  zoneData: PropTypes.object.isRequired,
  billingTypeData: PropTypes.object.isRequired
}

export default PickupGrid
