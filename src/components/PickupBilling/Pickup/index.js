import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Button, Card, Modal } from 'semantic-ui-react'
import { Formik } from 'formik'

import Layout from '../../Shared/Layout'
import FormWrapper from '../../Shared/FormWrapper'
import fieldValidation from '../../../helpers/FieldValidation'

import PickupGrid from './pickupGrid'
import { formFields, searchFormFields } from './fieldSchema'

const Pickup = props => {
  const {
      history,
      modalVisible,
      modalActionName,
      formSubmit,
      searchFormSubmit,
      selectedPickup,
      modalToggle
    } = props,
    initialValues =
      (modalActionName === 'edit' && {
        initialValues: {
          ...selectedPickup
        }
      }) ||
      {}

  return (
    <Fragment>
      <Layout history={history}>
        <Card fluid>
          <Card.Content>
            <Card.Header>Existing Pickups</Card.Header>
            <Button
              basic
              compact
              floated="right"
              onClick={() => modalToggle('create', modalVisible)}
            >
              Create
            </Button>
          </Card.Content>
          <Card.Content>
            <Formik
              initialValues={{}}
              validationSchema={fieldValidation(searchFormFields(props))}
              onSubmit={searchFormSubmit}
              component={() => (
                <FormWrapper
                  fields={searchFormFields(props)}
                  button={[
                    {
                      title: 'Search',
                      className: 'bg-primary-color',
                      type: 'submit'
                    }
                  ]}
                />
              )}
            />
          </Card.Content>
          <Card.Content>
            <PickupGrid {...props} />
          </Card.Content>
        </Card>
      </Layout>

      <Modal
        onClose={() => modalToggle('', modalVisible)}
        open={modalVisible}
        size="large"
        closeIcon
      >
        <Modal.Header>{modalActionName} Pickup</Modal.Header>
        <Modal.Content>
          <Formik
            {...initialValues}
            validationSchema={fieldValidation(formFields(props))}
            onSubmit={formSubmit}
            component={() => (
              <FormWrapper
                fields={formFields(props)}
                button={[
                  {
                    title: modalActionName,
                    className: 'bg-primary-color',
                    type: 'submit'
                  }
                ]}
              />
            )}
          />
        </Modal.Content>
      </Modal>
    </Fragment>
  )
}

Pickup.propTypes = {
  history: PropTypes.object.isRequired,
  modalVisible: PropTypes.bool.isRequired,
  modalActionName: PropTypes.string.isRequired,
  formSubmit: PropTypes.func.isRequired,
  searchFormSubmit: PropTypes.func.isRequired,
  modalToggle: PropTypes.func.isRequired
}

export default Pickup
