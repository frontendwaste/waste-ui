import Yup from 'yup'

function equalTo(ref, msg) {
  return Yup.mixed().test({
    name: 'equalTo',
    exclusive: false,
    message: msg || `Must be the same as ${ref.path}`,
    params: {
      reference: ref.path
    },
    test(value) {
      return value === this.resolve(ref)
    }
  })
}
Yup.addMethod(Yup.string, 'equalTo', equalTo)

export default formFields =>
  Yup.object().shape(
    formFields.reduce(
      (accum, { name, label, required, equalto }) =>
        required
          ? {
            ...accum,
            [name]: equalto
              ? Yup.string()
                .trim()
                .equalTo(
                  Yup.ref(equalto.key),
                  `Must be the same as ${equalto.name}`
                )
                .required(`${label} is required!`)
              : Yup.string()
                .trim()
                .required(`${label} is required!`)
          }
          : { ...accum },
      {}
    )
  )
