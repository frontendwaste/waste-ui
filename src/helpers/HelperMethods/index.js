import React from 'react'
import { Redirect } from 'react-router-dom'
import { getIn } from 'formik'

export const getRole = access => {
  if (
    access &&
    JSON.parse(localStorage.getItem('currentUser')) && // eslint-disable-line
    JSON.parse(localStorage.getItem('currentUser')).role // eslint-disable-line
  ) {
    const roles = JSON.parse(localStorage.getItem('currentUser')).role // eslint-disable-line
    return access.some(item => roles.includes(item))
  }
  return true
}

export const getCurrUser = () =>
  localStorage.getItem('currentUser') // eslint-disable-line
    ? JSON.parse(localStorage.getItem('currentUser')) // eslint-disable-line
    : null

export const paramGen = fieldObj => {
  const params = new URLSearchParams()

  Object.keys(fieldObj).map(item => params.append(item, fieldObj[item]))
  return params
}

export const checkActive = (item, history) =>
  item.path === history.location.pathname ? true : undefined

export const validateAll = value => {
  let error
  if (!/^(?! )[\W\S]*(?<! )$/i.test(value)) {
    error = 'No trailing/preceding whitespaces allowed'
  }
  return error
}

export const validateAlphabets = value => {
  let error
  if (!value) {
    error = 'Required'
  } else if (!/^([a-zA-Z-])+$/i.test(value)) {
    error = 'Can only consist of alphabets and hyphen(-)'
  } else if (!/^.{0,12}$/i.test(value)) {
    error = 'Cannot be more than 12 characters'
  }
  return error
}

export const validateJustAlpha = value => {
  let error
  if (!value) {
    error = 'Required'
  } else if (!/^([a-zA-Z-])+$/i.test(value)) {
    error = 'Can only consist of alphabets and hyphen(-)'
  }
  return error
}

export const validateEmail = value => {
  let error
  if (!value) {
    error = 'Required'
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = 'Invalid email address'
  }
  return error
}

export const validatePhone = value => {
  let error
  if (!value) {
    error = 'Required'
  } else if (!/^(\d)+$/i.test(value)) {
    error = 'Invalid phone number'
  } else if (!/^.{7,11}$/i.test(value)) {
    error = 'Must be between 7-11 digits'
  }
  return error
}

export const showError = ({ errors, touched, submitCount }, { name }) => {
  const error = getIn(errors, name)
  const touch = getIn(touched, name)

  if (submitCount < 1) {
    if (errors[name] && touched[name]) {
      return true
    } else if (error && touch) {
      return true
    }
    return false
  } else {
    if (errors[name]) {
      return true
    } else if (error) {
      return true
    }
    return false
  }
}

export function authenticatedPage(Component) {
  const componentName = Component.displayName || Component.name || 'Component'

  return class extends Component {
    static displayName = `Route(${componentName})`

    renderPage() {
      return <Component {...this.props} />
    }

    render() {
      const token = getCurrUser()
      if (token) {
        return this.renderPage()
      }
      return <Redirect to="/login" />
    }
  }
}
