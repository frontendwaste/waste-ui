import {
  TOGGLE_MODAL,
  LOAD_GRID_ITEM,
  UPLOAD_DATA,
  HANDLE_PAGING
} from '../constants/action-types'

const initialState = {
  modal: { action: '', visible: false, selectedItem: {} },
  uploadData: {},
  pagination: { currPage: 0, pageSize: 10, noOfPages: 1 },
  tenantId: localStorage.getItem('currentUser')
    ? JSON.parse(localStorage.getItem('currentUser')).tenant_id
    : '',
  userId: localStorage.getItem('currentUser')
    ? JSON.parse(localStorage.getItem('currentUser')).sub
    : ''
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case TOGGLE_MODAL:
      return {
        ...state,
        modal: {
          ...state.modal,
          ...payload
        }
      }
    case HANDLE_PAGING:
      return {
        ...state,
        pagination: {
          ...payload
        }
      }
    case LOAD_GRID_ITEM:
      return {
        ...state,
        modal: { ...state.modal, selectedItem: payload }
      }
    case UPLOAD_DATA:
      return {
        ...state,
        uploadData: payload
      }
    default:
      return state
  }
}
