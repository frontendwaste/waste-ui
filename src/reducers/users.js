import { FETCH_USER } from '../constants/action-types'

const initialState = {
  usersData: {}
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_USER:
      return {
        ...state,
        usersData: {
          data: payload.data
        }
      }
    default:
      return state
  }
}
