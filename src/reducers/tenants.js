import { FETCH_TENANT } from '../constants/action-types'

const initialState = {
  tenantsData: {}
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_TENANT:
      return {
        ...state,
        tenantsData: payload.data
      }
    default:
      return state
  }
}
