import {
  FETCH_BANK,
  FETCH_TRUCK,
  FETCH_PAYMENT_METHOD,
  FETCH_COUNTRY,
  FETCH_STATE,
  FETCH_CURRENCY,
  FETCH_ZONE,
  FETCH_BRANCH,
  FETCH_THEME,
  FETCH_COMMISSION
} from '../constants/action-types'

const initialState = {
  banksData: {},
  trucksData: {},
  paymentMethodsData: {},
  countryData: {},
  statesData: {},
  currencyData: {},
  zonesData: {},
  branchData: {},
  themeData: {},
  commissionData: {}
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_BANK:
      return {
        ...state,
        banksData: payload.data
      }
    case FETCH_TRUCK:
      return {
        ...state,
        trucksData: payload.data
      }
    case FETCH_PAYMENT_METHOD:
      return {
        ...state,
        paymentMethodsData: payload.data
      }
    case FETCH_COUNTRY:
      return {
        ...state,
        countryData: payload.data
      }
    case FETCH_STATE:
      return {
        ...state,
        statesData: payload.data
      }
    case FETCH_CURRENCY:
      return {
        ...state,
        currencyData: payload.data
      }
    case FETCH_ZONE:
      return {
        ...state,
        zonesData: payload.data
      }
    case FETCH_BRANCH:
      return {
        ...state,
        branchData: payload.data
      }
    case FETCH_THEME:
      return {
        ...state,
        themeData: payload.data
      }
    case FETCH_COMMISSION:
      return {
        ...state,
        commissionData: payload.data
      }

    default:
      return state
  }
}
