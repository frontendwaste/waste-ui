import {
  FETCH_CUSTOMER,
  FETCH_CUSTOMER_WALLET,
  FETCH_CUSTOMER_TYPE,
  FETCH_SUB_CUSTOMER_TYPE,
  FETCH_BILLING_TYPE
} from '../constants/action-types'

const initialState = {
  customerData: {},
  customerWalletData: {},
  customerTypeData: {},
  subCustomerTypeData: {},
  billingTypeData: {}
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_CUSTOMER:
      return {
        ...state,
        customerData: payload.data
      }
    case FETCH_CUSTOMER_WALLET:
      return {
        ...state,
        customerWalletData: payload.data
      }
    case FETCH_CUSTOMER_TYPE:
      return {
        ...state,
        customerTypeData: payload.data
      }
    case FETCH_SUB_CUSTOMER_TYPE:
      return {
        ...state,
        subCustomerTypeData: payload.data
      }
    case FETCH_BILLING_TYPE:
      return {
        ...state,
        billingTypeData: payload.data
      }
    default:
      return state
  }
}
