import { combineReducers } from 'redux'

import main from './main'
import users from './users'
import roles from './roles'
import system from './system'
import config from './config'
import payment from './payment'
import pickup from './pickup'
import tenants from './tenants'
import clients from './clients'

import { reducer as toastrReducer } from 'react-redux-toastr'
import { loadingBarReducer } from 'react-redux-loading-bar'

const rootReducer = combineReducers({
  main,
  system,
  config,
  payment,
  pickup,
  users,
  roles,
  tenants,
  clients,
  toastr: toastrReducer,
  loadingBar: loadingBarReducer
})

export default rootReducer
