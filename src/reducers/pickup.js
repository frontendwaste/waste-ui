import {
  FETCH_PICKUP,
  FETCH_BILLING,
  FETCH_PICKUP_STATUS
} from '../constants/action-types'

const initialState = {
  pickupData: {},
  statusData: {},
  billingData: {}
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_PICKUP:
      return {
        ...state,
        pickupData: payload.data
      }
    case FETCH_BILLING:
      return {
        ...state,
        billingData: payload.data
      }
    case FETCH_PICKUP_STATUS:
      return {
        ...state,
        statusData: payload.data
      }
    default:
      return state
  }
}
