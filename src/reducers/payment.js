import {
  FETCH_TRANSACTION,
  FETCH_CHANNEL,
  FETCH_PAYMENT_STATUS
} from '../constants/action-types'

const initialState = {
  transactionData: {},
  statusData: {},
  channelData: {}
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_TRANSACTION:
      return {
        ...state,
        transactionData: payload.data
      }
    case FETCH_CHANNEL:
      return {
        ...state,
        channelData: payload.data
      }
    case FETCH_PAYMENT_STATUS:
      return {
        ...state,
        statusData: payload.data
      }
    default:
      return state
  }
}
