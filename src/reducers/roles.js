import { FETCH_ROLE } from '../constants/action-types'

const initialState = {
  rolesData: {}
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_ROLE:
      return {
        ...state,
        rolesData: {
          data: payload.data
        }
      }
    default:
      return state
  }
}
