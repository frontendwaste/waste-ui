import { FETCH_CLIENT } from '../constants/action-types'

const initialState = {
  clientsData: {}
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case FETCH_CLIENT:
      return {
        ...state,
        clientsData: payload.data
      }
    default:
      return state
  }
}
