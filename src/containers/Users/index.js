import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import UserComponent from '../../components/Users'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  handlePaging
} from '../../actions'
import {
  FETCH_ROLE,
  FETCH_USER,
  FETCH_TENANT
} from '../../constants/action-types'
import {
  GET_USER,
  UPDATE_USER,
  CREATE_USER,
  GET_ROLE,
  GET_TENANT
} from '../../constants'

class Users extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: '',
      lastName: '',
      username: '',
      email: '',
      search: ''
    }
  }

  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
    this.props.getData(GET_ROLE, FETCH_ROLE)
    if (!this.props.tenantId) {
      this.props.getData(GET_TENANT, FETCH_TENANT)
    }
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    this.props
      .getData(GET_USER, FETCH_USER, {
        params: { ...this.state, pageNumber: page + 1, pageSize }
      })
      .then(result => {
        return (
          result &&
          result.data &&
          (result.data.TotalRecords &&
            this.props.handlePaging({
              currPage: page,
              noOfPages: Math.ceil(result.data.TotalRecords / pageSize),
              pageSize
            }))
        )
      })
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.props.getData(`${GET_USER}/${id}`, 'GET').then(result => {
        this.props.loadGridItem(id, data, result.data)
        this.modalToggle(action, modalVisible)
      })
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  searchFormSubmit(
    { firstName, lastName, username, email, search },
    { setSubmitting }
  ) {
    this.setState(prevState => ({
      ...prevState,
      firstName,
      lastName,
      username,
      email,
      search
    }))
    this.fetchGridData({ page: 0, pageSize: 10 })

    setSubmitting(false)
  }

  formSubmit(
    {
      id,
      email,
      firstName,
      lastName,
      username,
      mobileNumber,
      tenantId,
      alternativeMobileNumber,
      roleId,
      enabled
    },
    { setSubmitting }
  ) {
    const { postData, putData } = this.props,
      data = {
        email,
        firstName,
        lastName,
        tenantId: this.props.tenantId ? this.props.tenantId : tenantId,
        mobileNumber,
        roleId,
        alternativeMobileNumber,
        enabled
      },
      url = id ? UPDATE_USER + id : CREATE_USER

    if (id) {
      putData(url, { ...data, id }).then(
        result => result && this.fetchGridData({})
      )
    } else {
      postData(url, { ...data, username }).then(
        result => result && this.fetchGridData({})
      )
    }

    setSubmitting(false)
  }

  render() {
    return (
      <UserComponent
        {...this.props}
        fetchGridData={this.fetchGridData.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        searchFormSubmit={this.searchFormSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    selectedUser: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    tenantId: state.main.tenantId,
    tenantsData: state.tenants.tenantsData,
    usersData: state.users.usersData,
    rolesData: state.roles.rolesData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      loadGridItem,
      getData,
      putData,
      postData,
      handlePaging
    },
    dispatch
  )
}

Users.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired,
  tenantId: PropTypes.string
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Users)
