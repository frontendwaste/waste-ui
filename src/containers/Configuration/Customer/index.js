import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import FileSaver from 'file-saver'

import CustomerComponent from '../../../components/Configuration/Customer'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  uploadingData,
  requestDataSuccess,
  handlePaging
} from '../../../actions'
import {
  FETCH_CUSTOMER,
  FETCH_CUSTOMER_WALLET,
  FETCH_CUSTOMER_TYPE,
  FETCH_ZONE,
  FETCH_BRANCH
} from '../../../constants/action-types'
import {
  GET_BRANCH,
  GET_WALLET,
  SEARCH_CUSTOMER,
  UPDATE_CUSTOMER,
  CREATE_CUSTOMER,
  UPLOAD_CUSTOMER,
  GET_CUSTOMER_TYPE,
  GET_CUSTOMER_TEMPLATE,
  GET_ZONE
} from '../../../constants'

class Customer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      zoneId: '',
      customerTypeId: '',
      isTagged: '',
      startDate: '',
      endDate: ''
    }

    this.fetchGridData = this.fetchGridData.bind(this)
    this.formSubmit = this.formSubmit.bind(this)
    this.downloadTemplate = this.downloadTemplate.bind(this)
    this.uploadFormSubmit = this.uploadFormSubmit.bind(this)
    this.searchFormSubmit = this.searchFormSubmit.bind(this)
    this.actionClick = this.actionClick.bind(this)
    this.modalToggle = this.modalToggle.bind(this)
  }

  componentDidMount() {
    const { getData } = this.props

    this.fetchGridData({ page: 0, pageSize: 10 })
    getData(GET_CUSTOMER_TYPE, FETCH_CUSTOMER_TYPE)
    getData(GET_ZONE, FETCH_ZONE)
    getData(GET_BRANCH, FETCH_BRANCH)
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    const data = {
      ...this.state,
      page: page + 1,
      limit: pageSize
    }
    this.props.postData(SEARCH_CUSTOMER, data).then(result => {
      if (result && result.data.data) {
        this.props.requestDataSuccess(result, FETCH_CUSTOMER)
        this.props.handlePaging({
          currPage: page,
          noOfPages: Math.ceil(result.data.data.totalRecords / pageSize),
          pageSize
        })
      }
    })
  }

  actionClick(id, action, data, modalVisible) {
    const { loadGridItem, postData, requestDataSuccess } = this.props

    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      loadGridItem(id, data)
    } else if (action === 'wallet') {
      const dataObj = { userId: id }

      postData(GET_WALLET, dataObj).then(result => {
        if (result && result.data) {
          requestDataSuccess(result, FETCH_CUSTOMER_WALLET)
          this.modalToggle(action, modalVisible)
        }
      })
    }
  }

  modalToggle(action, modalVisible) {
    const { toggleModal } = this.props

    toggleModal(action, modalVisible)
  }

  searchFormSubmit(
    { zoneId, customerTypeId, isTagged, startDate, endDate },
    { setSubmitting }
  ) {
    this.setState(prevState => ({
      ...prevState,
      zoneId,
      customerTypeId,
      isTagged,
      startDate,
      endDate
    }))
    this.fetchGridData({})

    setSubmitting(false)
  }

  formSubmit(
    {
      id,
      firstName,
      lastName,
      email,
      phoneNumber,
      baseAmount,
      zoneId,
      isTagged,
      customerTypeId,
      branchId,
      address
    },
    { setSubmitting }
  ) {
    const { postData, putData } = this.props,
      data = {
        firstName,
        lastName,
        email,
        phoneNumber,
        baseAmount,
        zoneId,
        isTagged: isTagged ? '1' : '0',
        customerTypeId,
        branchId,
        address
      },
      url = id ? UPDATE_CUSTOMER + id : CREATE_CUSTOMER

    if (id) {
      putData(url, data).then(result => result && this.fetchGridData({}))
    } else {
      postData(url, data).then(result => result && this.fetchGridData({}))
    }

    setSubmitting(false)
  }

  downloadTemplate() {
    const config = { responseType: 'blob' }
    this.props.getData(GET_CUSTOMER_TEMPLATE, 'FETCH', config).then(result => {
      result && FileSaver.saveAs(result.data, 'Customer Upload')
    })
  }

  uploadFormSubmit(
    { customerTypeId, zoneId, branchId },
    { setSubmitting },
    uploadData
  ) {
    const { postData } = this.props
    let data = new FormData()

    if (uploadData.name && uploadData.name !== '') {
      data.append('customerTypeId', customerTypeId)
      data.append('zoneId', zoneId)
      data.append('branchId', branchId)
      data.append('customers', uploadData, uploadData.name)

      postData(UPLOAD_CUSTOMER, data).then(() => this.fetchGridData({}))
    }

    setSubmitting(false)
  }

  render() {
    return (
      <CustomerComponent
        {...this.props}
        fetchGridData={this.fetchGridData}
        formSubmit={this.formSubmit}
        downloadTemplate={this.downloadTemplate}
        uploadFormSubmit={this.uploadFormSubmit}
        searchFormSubmit={this.searchFormSubmit}
        actionClick={this.actionClick}
        modalToggle={this.modalToggle}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    uploadData: state.main.uploadData,
    selectedCustomer: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    customerData: state.config.customerData,
    customerWalletData: state.config.customerWalletData,
    customerTypeData: state.config.customerTypeData,
    zoneData: state.system.zonesData,
    branchData: state.system.branchData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      loadGridItem,
      getData,
      putData,
      postData,
      uploadingData,
      requestDataSuccess,
      handlePaging
    },
    dispatch
  )
}

Customer.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  requestDataSuccess: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Customer)
