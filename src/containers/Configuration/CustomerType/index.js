import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import CustomerTypeComponent from '../../../components/Configuration/CustomerType'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  handlePaging
} from '../../../actions'
import { FETCH_CUSTOMER_TYPE } from '../../../constants/action-types'
import {
  GET_CUSTOMER_TYPE,
  UPDATE_CUSTOMER_TYPE,
  CREATE_CUSTOMER_TYPE
} from '../../../constants'

class CustomerType extends Component {
  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    this.props
      .getData(GET_CUSTOMER_TYPE, FETCH_CUSTOMER_TYPE, {
        params: { page: page + 1, size: pageSize }
      })
      .then(result => {
        return result && result.data && (
          result.data.totalRecords &&
          this.props.handlePaging({
            currPage: page,
            noOfPages: Math.ceil(result.data.totalRecords / pageSize),
            pageSize
          })
        )
      })
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  formSubmit({ id, name }, { setSubmitting }) {
    const { postData, putData } = this.props,
      data = {
        name,
        tenantId: this.props.tenantId
      },
      url = id ? UPDATE_CUSTOMER_TYPE + id : CREATE_CUSTOMER_TYPE

    if (id) {
      putData(url, data).then(result => result && this.fetchGridData({}))
    } else {
      postData(url, data).then(result => result && this.fetchGridData({}))
    }

    setSubmitting(false)
  }

  render() {
    return (
      <CustomerTypeComponent
        {...this.props}
        fetchGridData={this.fetchGridData.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    selectedCustomerType: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    tenantId: state.main.tenantId,
    customerTypeData: state.config.customerTypeData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      loadGridItem,
      getData,
      putData,
      postData,
      handlePaging
    },
    dispatch
  )
}

CustomerType.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired,
  tenantId: PropTypes.string.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerType)
