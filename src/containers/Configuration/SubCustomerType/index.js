import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import SubCustomerTypeComponent from '../../../components/Configuration/SubCustomerType'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  handlePaging
} from '../../../actions'
import {
  FETCH_SUB_CUSTOMER_TYPE,
  FETCH_CUSTOMER_TYPE,
  FETCH_BILLING_TYPE
} from '../../../constants/action-types'
import {
  GET_SUB_CUSTOMER_TYPE,
  UPDATE_SUB_CUSTOMER_TYPE,
  CREATE_SUB_CUSTOMER_TYPE,
  GET_CUSTOMER_TYPE,
  GET_BILLING_TYPE
} from '../../../constants'

class SubCustomerType extends Component {
  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
    this.props.getData(GET_CUSTOMER_TYPE, FETCH_CUSTOMER_TYPE)
    this.props.getData(GET_BILLING_TYPE, FETCH_BILLING_TYPE)
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    this.props
      .getData(GET_SUB_CUSTOMER_TYPE, FETCH_SUB_CUSTOMER_TYPE, {
        params: { page: page + 1, size: pageSize }
      })
      .then(result => {
        return result && result.data && (
          result.data.totalRecords &&
          this.props.handlePaging({
            currPage: page,
            noOfPages: Math.ceil(result.data.totalRecords / pageSize),
            pageSize
          })
        )
      })
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  formSubmit(
    { id, name, customerTypeId, billingTypeId, baseAmount },
    { setSubmitting }
  ) {
    const { postData, putData } = this.props,
      data = {
        name,
        customerTypeId,
        billingTypeId,
        baseAmount
      },
      url = id ? UPDATE_SUB_CUSTOMER_TYPE + id : CREATE_SUB_CUSTOMER_TYPE

    if (id) {
      putData(url, data).then(result => result && this.fetchGridData({}))
    } else {
      postData(url, data).then(result => result && this.fetchGridData({}))
    }

    setSubmitting(false)
  }

  render() {
    return (
      <SubCustomerTypeComponent
        {...this.props}
        fetchGridData={this.fetchGridData.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    selectedSubCustomerType: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    subCustomerTypeData: state.config.subCustomerTypeData,
    customerTypeData: state.config.customerTypeData,
    billingTypeData: state.config.billingTypeData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      loadGridItem,
      getData,
      putData,
      postData,
      handlePaging
    },
    dispatch
  )
}

SubCustomerType.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubCustomerType)
