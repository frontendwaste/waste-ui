import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import PickupComponent from '../../../components/PickupBilling/Pickup'

import {
  toggleModal,
  getData,
  postData,
  putData,
  loadGridItem,
  requestDataSuccess,
  handlePaging
} from '../../../actions'
import {
  FETCH_PICKUP,
  FETCH_PICKUP_STATUS,
  FETCH_CUSTOMER,
  FETCH_USER,
  FETCH_ZONE,
  FETCH_BILLING_TYPE
} from '../../../constants/action-types'
import {
  SEARCH_PICKUP,
  CREATE_PICKUP,
  UPDATE_PICKUP,
  GET_PICKUP_STATUS,
  GET_ZONE,
  GET_USER,
  GET_CUSTOMER,
  GET_BILLING_TYPE
} from '../../../constants'

class Pickup extends Component {
  constructor(props) {
    super(props)
    this.state = {
      customerCode: '',
      userId: '',
      zoneId: '',
      pickupDate: '',
      statusId: '',
      startDate: '',
      endDate: '',
      billingTypeId: ''
    }
  }

  componentDidMount() {
    const { getData } = this.props

    this.fetchGridData({ page: 0, pageSize: 10 })
    getData(GET_PICKUP_STATUS, FETCH_PICKUP_STATUS)
    getData(GET_ZONE, FETCH_ZONE)
    getData(GET_USER, FETCH_USER)
    getData(GET_CUSTOMER, FETCH_CUSTOMER)
    getData(GET_BILLING_TYPE, FETCH_BILLING_TYPE)
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    const data = {
      ...this.state,
      page: page + 1,
      limit: pageSize
    }
    this.props.postData(SEARCH_PICKUP, data).then(result => {
      if (result && result.data.data) {
        this.props.requestDataSuccess(result, FETCH_PICKUP)
        this.props.handlePaging({
          currPage: page,
          noOfPages: Math.ceil(result.data.data.totalRecords / pageSize),
          pageSize
        })
      }
    })
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  searchFormSubmit(
    {
      customerCode,
      userId,
      zoneId,
      pickupDate,
      statusId,
      startDate,
      endDate,
      billingTypeId
    },
    { setSubmitting }
  ) {
    this.setState(prevState => ({
      ...prevState,
      customerCode,
      userId,
      zoneId,
      pickupDate,
      statusId,
      startDate,
      endDate,
      billingTypeId
    }))
    this.fetchGridData({})

    setSubmitting(false)
  }

  formSubmit(
    { id, customerCode, userId, zoneId, pickupDate, billingTypeId, statusId },
    { setSubmitting }
  ) {
    const { postData, putData } = this.props,
      data = {
        customerCode,
        userId,
        zoneId,
        pickupDate,
        billingTypeId,
        statusId
      },
      url = id ? UPDATE_PICKUP + id : CREATE_PICKUP

    if (id) {
      putData(url, data).then(result => result && this.fetchGridData({}))
    } else {
      postData(url, data).then(result => result && this.fetchGridData({}))
    }

    setSubmitting(false)
  }

  render() {
    return (
      <PickupComponent
        {...this.props}
        fetchGridData={this.fetchGridData.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        searchFormSubmit={this.searchFormSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    uploadData: state.main.uploadData,
    selectedPickup: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    pickupData: state.pickup.pickupData,
    customerData: state.config.customerData,
    zoneData: state.system.zonesData,
    userData: state.users.usersData,
    billingTypeData: state.config.billingTypeData,
    statusData: state.pickup.statusData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      getData,
      loadGridItem,
      postData,
      putData,
      requestDataSuccess,
      handlePaging
    },
    dispatch
  )
}

Pickup.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  requestDataSuccess: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Pickup)
