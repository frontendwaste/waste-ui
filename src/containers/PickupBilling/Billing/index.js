import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import BillingComponent from '../../../components/PickupBilling/Billing'

import {
  getData,
  postData,
  requestDataSuccess,
  handlePaging
} from '../../../actions'
import {
  FETCH_BILLING,
  FETCH_PICKUP_STATUS,
  FETCH_CUSTOMER,
  FETCH_USER,
  FETCH_ZONE,
  FETCH_BILLING_TYPE
} from '../../../constants/action-types'
import {
  GET_BILLING,
  GET_PICKUP_STATUS,
  GET_ZONE,
  GET_USER,
  GET_CUSTOMER,
  GET_BILLING_TYPE
} from '../../../constants'

class Billing extends Component {
  constructor(props) {
    super(props)
    this.state = {
      customerCode: '',
      userId: '',
      zoneId: '',
      pickupDate: '',
      statusId: '',
      startDate: '',
      endDate: '',
      billingTypeId: ''
    }
  }

  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
    this.props.getData(GET_PICKUP_STATUS, FETCH_PICKUP_STATUS)
    this.props.getData(GET_ZONE, FETCH_ZONE)
    this.props.getData(GET_USER, FETCH_USER)
    this.props.getData(GET_CUSTOMER, FETCH_CUSTOMER)
    this.props.getData(GET_BILLING_TYPE, FETCH_BILLING_TYPE)
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    const data = {
      ...this.state,
      page: page + 1,
      limit: pageSize
    }
    this.props.postData(GET_BILLING, data).then(result => {
      if (result && result.data) {
        this.props.requestDataSuccess(result, FETCH_BILLING)
        result.data.totalRecords && this.props.handlePaging({
          currPage: page,
          noOfPages: Math.ceil(result.data.totalRecords / pageSize),
          pageSize
        })
      }
    })
  }

  searchFormSubmit(
    {
      customerCode,
      userId,
      zoneId,
      pickupDate,
      statusId,
      startDate,
      endDate,
      billingTypeId
    },
    { setSubmitting }
  ) {
    this.setState(prevState => ({
      ...prevState,
      customerCode,
      userId,
      zoneId,
      pickupDate,
      statusId,
      startDate,
      endDate,
      billingTypeId
    }))
    this.fetchGridData({})

    setSubmitting(false)
  }

  render() {
    return (
      <BillingComponent
        {...this.props}
        searchFormSubmit={this.searchFormSubmit.bind(this)}
        fetchGridData={this.fetchGridData.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    pagination: state.main.pagination,
    billingData: state.pickup.billingData,
    customerData: state.config.customerData,
    userData: state.users.usersData,
    zoneData: state.system.zonesData,
    statusData: state.pickup.statusData,
    billingTypeData: state.config.billingTypeData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      getData,
      postData,
      handlePaging,
      requestDataSuccess
    },
    dispatch
  )
}

Billing.propTypes = {
  getData: PropTypes.func.isRequired,
  requestDataSuccess: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Billing)
