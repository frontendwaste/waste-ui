import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import RoleComponent from '../../components/Roles'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  handlePaging
} from '../../actions'
import { FETCH_ROLE } from '../../constants/action-types'
import { GET_ROLE, UPDATE_ROLE, CREATE_ROLE } from '../../constants'

class Roles extends Component {
  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    this.props
      .getData(GET_ROLE, FETCH_ROLE, {
        params: { page: page + 1, size: pageSize }
      })
      .then(result => {
        return result && result.data && (
          result.data.TotalRecords &&
          this.props.handlePaging({
            currPage: page,
            noOfPages: Math.ceil(result.data.TotalRecords / pageSize),
            pageSize
          })
        )
      })
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  formSubmit({ id, name }, { setSubmitting }) {
    const { postData, putData } = this.props,
      data = {
        name
      },
      url = id ? UPDATE_ROLE + id : CREATE_ROLE

    if (id) {
      putData(url, data).then(result => result && this.fetchGridData({}))
    } else {
      postData(url, data).then(result => result && this.fetchGridData({}))
    }

    setSubmitting(false)
  }

  render() {
    return (
      <RoleComponent
        {...this.props}
        fetchGridData={this.fetchGridData.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    selectedRole: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    rolesData: state.roles.rolesData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      loadGridItem,
      getData,
      putData,
      postData,
      handlePaging
    },
    dispatch
  )
}

Roles.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Roles)
