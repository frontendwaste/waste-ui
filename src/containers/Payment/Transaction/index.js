import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import TransactionComponent from '../../../components/Payment/Transaction'

import {
  toggleModal,
  getData,
  postData,
  requestDataSuccess,
  handlePaging
} from '../../../actions'
import {
  FETCH_TRANSACTION,
  FETCH_USER,
  FETCH_PAYMENT_STATUS,
  FETCH_CHANNEL
} from '../../../constants/action-types'
import {
  SEARCH_TRANSACTIONS,
  VALIDATE_TRANSACTION,
  VOID_TRANSACTION,
  CREATE_TRANSACTION,
  GET_USER,
  GET_PAYMENT_STATUS,
  GET_CHANNEL
} from '../../../constants'

class Transaction extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userId: '',
      channelId: '',
      channelName: '',
      statusId: '',
      startDate: '',
      endDate: ''
    }
  }

  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
    this.props.getData(GET_USER, FETCH_USER)
    this.props.getData(GET_PAYMENT_STATUS, FETCH_PAYMENT_STATUS)
    this.props.getData(GET_CHANNEL, FETCH_CHANNEL)
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    const data = {
      ...this.state,
      page: page + 1,
      limit: pageSize
    }
    this.props.postData(SEARCH_TRANSACTIONS, data).then(result => {
      if (result && result.data.data) {
        this.props.requestDataSuccess(result, FETCH_TRANSACTION)
        this.props.handlePaging({
          currPage: page,
          noOfPages: Math.ceil(result.data.data.totalRecords / pageSize),
          pageSize
        })
      }
    })
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  searchFormSubmit(
    { userId, channelId, channelName, statusId, startDate, endDate },
    { setSubmitting }
  ) {
    this.setState(prevState => ({
      ...prevState,
      userId,
      channelId,
      channelName,
      statusId,
      startDate,
      endDate
    }))
    this.fetchGridData({})

    setSubmitting(false)
  }

  formSubmit({ amount, userId, channelId }, { setSubmitting }) {
    const { postData } = this.props,
      data = {
        amount,
        userId,
        channelId
      }

    postData(CREATE_TRANSACTION, data).then(
      result => result && this.fetchGridData({})
    )

    setSubmitting(false)
  }

  voidTransaction(transactionId) {
    const { postData } = this.props

    if (transactionId) {
      postData(VOID_TRANSACTION, { transactionId }).then(
        result => result && this.fetchGridData({})
      )
    }
  }

  validateTransaction(transactionId) {
    const { postData } = this.props

    if (transactionId) {
      postData(VALIDATE_TRANSACTION, { transactionId }).then(
        result => result && this.fetchGridData({})
      )
    }
  }

  render() {
    return (
      <TransactionComponent
        {...this.props}
        fetchGridData={this.fetchGridData.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        voidTransaction={this.voidTransaction.bind(this)}
        validateTransaction={this.validateTransaction.bind(this)}
        searchFormSubmit={this.searchFormSubmit.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    uploadData: state.main.uploadData,
    selectedCustomer: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    transactionData: state.payment.transactionData,
    userData: state.users.usersData,
    channelData: state.payment.channelData,
    statusData: state.payment.statusData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      getData,
      postData,
      requestDataSuccess,
      handlePaging
    },
    dispatch
  )
}

Transaction.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  requestDataSuccess: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Transaction)
