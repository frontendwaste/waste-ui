import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import ThemeComponent from '../../../components/System/Theme'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  uploadingData,
  handlePaging
} from '../../../actions'
import { FETCH_THEME } from '../../../constants/action-types'
import { GET_THEME, UPDATE_THEME, CREATE_THEME } from '../../../constants'
import fonts from './fonts.json'

class Theme extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fonts: fonts
    }
  }

  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    this.props
      .getData(GET_THEME, FETCH_THEME, {
        params: { page: page + 1, size: pageSize }
      })
      .then(result => {
        return (
          result &&
          result.data &&
          (result.data.totalRecords &&
            this.props.handlePaging({
              currPage: page,
              noOfPages: Math.ceil(result.data.totalRecords / pageSize),
              pageSize
            }))
        )
      })
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  formSubmit({ id, color, fonts, status }, { setSubmitting }, uploadData) {
    const { postData, putData } = this.props,
      url = id ? UPDATE_THEME + id : CREATE_THEME
    let data = new FormData()

    if (uploadData.name && uploadData.name !== '') {
      data.append('tenid', this.props.tenantId)
      data.append('color', color)
      data.append('fonts', fonts)
      data.append('status', status ? 'enable' : 'disable')
      data.append('logo', uploadData, uploadData.name)

      if (id) {
        putData(url, data).then(result => result && this.fetchGridData({}))
      } else {
        postData(url, data).then(result => result && this.fetchGridData({}))
      }
    }

    setSubmitting(false)
  }

  render() {
    return (
      <ThemeComponent
        {...this.props}
        fonts={this.state.fonts}
        fetchGridData={this.fetchGridData.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    uploadData: state.main.uploadData,
    selectedTheme: state.main.modal.selectedItem,
    tenantId: state.main.tenantId,
    pagination: state.main.pagination,
    themeData: state.system.themeData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      loadGridItem,
      getData,
      putData,
      postData,
      uploadingData,
      handlePaging
    },
    dispatch
  )
}

Theme.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired,
  tenantId: PropTypes.string.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Theme)
