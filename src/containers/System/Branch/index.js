import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import BranchComponent from '../../../components/System/Branch'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  requestDataSuccess,
  handlePaging
} from '../../../actions'
import {
  FETCH_COUNTRY,
  FETCH_STATE,
  FETCH_BRANCH
} from '../../../constants/action-types'
import {
  GET_BRANCH,
  UPDATE_BRANCH,
  CREATE_BRANCH,
  GET_COUNTRY,
  GET_STATE
} from '../../../constants'

class Branch extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stateDataFiltered: []
    }
  }

  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
    this.props.getData(GET_COUNTRY, FETCH_COUNTRY)
    this.props.getData(GET_STATE, FETCH_STATE).then(
      result =>
        result &&
        this.setState(state => ({
          ...state,
          stateDataFiltered: result.data.records
        }))
    )
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    this.props
      .getData(GET_BRANCH, FETCH_BRANCH, {
        params: { page: page + 1, size: pageSize }
      })
      .then(result => {
        return result && result.data && (
          result.data.totalRecords &&
          this.props.handlePaging({
            currPage: page,
            noOfPages: Math.ceil(result.data.totalRecords / pageSize),
            pageSize
          })
        )
      })
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  getState(countryId) {
    this.setState(state => ({
      ...state,
      stateDataFiltered: this.props.stateData.records.filter(
        state => state.country_id === countryId
      )
    }))
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  formSubmit({ id, name, code, states_id, status }, { setSubmitting }) {
    const { postData, putData } = this.props,
      data = {
        name,
        code,
        tenid: this.props.tenantId,
        states_id,
        status: status ? 'enable' : 'disable'
      },
      url = id ? UPDATE_BRANCH + id : CREATE_BRANCH

    if (id) {
      putData(url, data).then(result => result && this.fetchGridData({}))
    } else {
      postData(url, data).then(result => result && this.fetchGridData({}))
    }

    setSubmitting(false)
  }

  render() {
    return (
      <BranchComponent
        {...this.props}
        stateDataFiltered={this.state.stateDataFiltered}
        fetchGridData={this.fetchGridData.bind(this)}
        getState={this.getState.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    selectedBranch: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    tenantId: state.main.tenantId,
    branchData: state.system.branchData,
    countryData: state.system.countryData,
    stateData: state.system.statesData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      loadGridItem,
      getData,
      putData,
      postData,
      requestDataSuccess,
      handlePaging
    },
    dispatch
  )
}

Branch.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  requestDataSuccess: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired,
  stateData: PropTypes.object.isRequired,
  tenantId: PropTypes.string.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Branch)
