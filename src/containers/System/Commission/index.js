import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import CommissionComponent from '../../../components/System/Commission'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData
} from '../../../actions'
import { FETCH_COMMISSION } from '../../../constants/action-types'
import {
  GET_COMMISSION,
  UPDATE_COMMISSION,
  CREATE_COMMISSION
} from '../../../constants'

class Commission extends Component {
  componentDidMount() {
    this.props.getData(GET_COMMISSION, FETCH_COMMISSION)
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  formSubmit({ id, name, code, tenid, states_id, status }, { setSubmitting }) {
    const { postData, putData, getData } = this.props,
      data = {
        name,
        code,
        tenid,
        states_id,
        status: status ? 'enable' : 'disable'
      },
      url = id ? UPDATE_COMMISSION + id : CREATE_COMMISSION

    if (id) {
      putData(url, data).then(result => result && getData(GET_COMMISSION))
    } else {
      postData(url, data).then(result => result && getData(GET_COMMISSION))
    }

    setSubmitting(false)
  }

  render() {
    return (
      <CommissionComponent
        {...this.props}
        formSubmit={this.formSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    selectedCommission: state.main.modal.selectedItem,
    commissionData: state.commission.commissionData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { toggleModal, loadGridItem, getData, putData, postData },
    dispatch
  )
}

Commission.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Commission)
