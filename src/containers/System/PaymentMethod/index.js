import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import PaymentMethodComponent from '../../../components/System/PaymentMethod'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  handlePaging
} from '../../../actions'
import {
  FETCH_BRANCH,
  FETCH_PAYMENT_METHOD
} from '../../../constants/action-types'
import {
  GET_PAYMENT_METHOD,
  UPDATE_PAYMENT_METHOD,
  CREATE_PAYMENT_METHOD,
  GET_BRANCH
} from '../../../constants'

class PaymentMethod extends Component {
  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
    this.props.getData(GET_BRANCH, FETCH_BRANCH)
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    this.props
      .getData(GET_PAYMENT_METHOD, FETCH_PAYMENT_METHOD, {
        params: { page: page + 1, size: pageSize }
      })
      .then(result => {
        return result && result.data && (
          result.data.totalRecords &&
          this.props.handlePaging({
            currPage: page,
            noOfPages: Math.ceil(result.data.totalRecords / pageSize),
            pageSize
          })
        )
      })
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  formSubmit({ id, type, branch_id, name, code, status }, { setSubmitting }) {
    const { postData, putData } = this.props,
      data = {
        type: type ? 'online' : 'offline',
        branch_id,
        name,
        code,
        status: status ? 'enable' : 'disable'
      },
      url = id ? UPDATE_PAYMENT_METHOD + id : CREATE_PAYMENT_METHOD

    if (id) {
      putData(url, data).then(result => result && this.fetchGridData({}))
    } else {
      postData(url, data).then(result => result && this.fetchGridData({}))
    }

    setSubmitting(false)
  }

  render() {
    return (
      <PaymentMethodComponent
        {...this.props}
        fetchGridData={this.fetchGridData.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    selectedPaymentMethod: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    paymentMethodsData: state.system.paymentMethodsData,
    branchData: state.system.branchData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { toggleModal, loadGridItem, getData, putData, postData, handlePaging },
    dispatch
  )
}

PaymentMethod.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentMethod)
