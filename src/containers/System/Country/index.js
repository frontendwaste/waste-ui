import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import CountryComponent from '../../../components/System/Country'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  handlePaging
} from '../../../actions'
import { FETCH_COUNTRY } from '../../../constants/action-types'
import { GET_COUNTRY, UPDATE_COUNTRY, CREATE_COUNTRY } from '../../../constants'

class Country extends Component {
  componentDidMount() {
    this.fetchGridData({ page: 0, pageSize: 10 })
  }

  fetchGridData({
    page = this.props.pagination.currPage,
    pageSize = this.props.pagination.pageSize
  }) {
    this.props
      .getData(GET_COUNTRY, FETCH_COUNTRY, {
        params: { page: page + 1, size: pageSize }
      })
      .then(result => {
        return result && result.data && (
          result.data.totalRecords &&
          this.props.handlePaging({
            currPage: page,
            noOfPages: Math.ceil(result.data.totalRecords / pageSize),
            pageSize
          })
        )
      })
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  formSubmit({ id, name, status }, { setSubmitting }) {
    const { postData, putData } = this.props,
      data = {
        name,
        status: status ? 'enable' : 'disable'
      },
      url = id ? UPDATE_COUNTRY + id : CREATE_COUNTRY

    if (id) {
      putData(url, data).then(result => result && this.fetchGridData({}))
    } else {
      postData(url, data).then(result => result && this.fetchGridData({}))
    }

    setSubmitting(false)
  }

  render() {
    return (
      <CountryComponent
        {...this.props}
        fetchGridData={this.fetchGridData.bind(this)}
        formSubmit={this.formSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    selectedCountry: state.main.modal.selectedItem,
    pagination: state.main.pagination,
    countryData: state.system.countryData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    { toggleModal, loadGridItem, getData, putData, postData, handlePaging },
    dispatch
  )
}

Country.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  handlePaging: PropTypes.func.isRequired,
  pagination: PropTypes.object.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Country)
