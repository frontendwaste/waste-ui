import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import ChangePasswordComponent from '../../../components/Account/ChangePassword'

import { postData } from '../../../actions'
import { CHANGE_PASSWORD } from '../../../constants'

class ChangePassword extends Component {
  formSubmit(values, { setSubmitting }) {
    const { postData } = this.props,
      data = {
        ...values
      }

    postData(CHANGE_PASSWORD, data)

    setSubmitting(false)
  }

  render() {
    return (
      <ChangePasswordComponent
        {...this.props}
        formSubmit={this.formSubmit.bind(this)}
      />
    )
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      postData
    },
    dispatch
  )
}

ChangePassword.propTypes = {
  postData: PropTypes.func.isRequired
}

export default connect(
  null,
  mapDispatchToProps
)(ChangePassword)
