import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import qs from 'qs'

import ResetPasswordComponent from '../../../components/Account/ResetPassword'

import { putData } from '../../../actions'
import { RESET_PASSWORD } from '../../../constants'

class ResetPassword extends Component {
  componentWillMount() {
    const { history, location } = this.props,
      token = localStorage.getItem('token'),
      search = qs.parse(location.search.slice(1))

    if (token) {
      history.replace('/')
    }
    if (!search.token) {
      history.replace('/')
    }
  }

  formSubmit({ password }, { setSubmitting }) {
    const { putData, history, location } = this.props,
      search = qs.parse(location.search.slice(1)),
      { token } = search,
      data = {
        token,
        password
      }

    putData(RESET_PASSWORD, data).then(() => {
      setTimeout(() => history.replace('/'), 1000)
    })

    setSubmitting(false)
  }

  render() {
    return (
      <ResetPasswordComponent
        {...this.props}
        formSubmit={this.formSubmit.bind(this)}
      />
    )
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      putData
    },
    dispatch
  )
}

ResetPassword.propTypes = {
  history: PropTypes.any.isRequired,
  location: PropTypes.any.isRequired,
  putData: PropTypes.func.isRequired
}

export default connect(
  null,
  mapDispatchToProps
)(ResetPassword)
