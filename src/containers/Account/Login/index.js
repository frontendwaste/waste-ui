import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import LoginComponent from '../../../components/Account/Login'

import { postData } from '../../../actions'
import { LOGIN } from '../../../constants'

const parseJwt = token => {
  try {
    return JSON.parse(atob(token.split('.')[1]))
  } catch (e) {
    return null
  }
}

class Login extends Component {
  componentWillMount() {
    const token = localStorage.getItem('token')
    if (token) {
      this.props.history.replace('/')
    }
  }

  formSubmit({ username, password }, { setSubmitting }) {
    const { postData, history } = this.props
    let params = new URLSearchParams()
    params.append('username', username)
    params.append('password', password)
    params.append('grant_type', 'password')

    const apiConfig = {
      headers: {
        Authorization:
          'Basic d2ViLWNsaWVudDo4YTI1NzYxMy1kZTYyLTQzZGItYjE0NS0wZjBmM2QyYWJmYmU='
      }
    }

    postData(LOGIN, params, apiConfig).then(res => {
      if (res) {
        localStorage.setItem('token', res.data.access_token)
        localStorage.setItem('currentUser', JSON.stringify(parseJwt(res.data.access_token)))
        //console.log(JSON.parse(localStorage.getItem('currentUser')))
        history.replace({
          pathname: '/'
        })
      }
    })

    setSubmitting(false)
  }

  render() {
    return (
      <LoginComponent {...this.props} formSubmit={this.formSubmit.bind(this)} />
    )
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      postData
    },
    dispatch
  )
}

Login.propTypes = {
  history: PropTypes.any.isRequired,
  postData: PropTypes.func.isRequired
}

export default connect(
  null,
  mapDispatchToProps
)(Login)
