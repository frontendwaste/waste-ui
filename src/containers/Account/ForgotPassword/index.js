import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import ForgotPasswordComponent from '../../../components/Account/ForgotPassword'

import { putData } from '../../../actions'
import { FORGOT_PASSWORD } from '../../../constants'

class ForgotPassword extends Component {
  componentWillMount() {
    const token = localStorage.getItem('token')
    if (token) {
      this.props.history.replace('/')
    }
  }

  formSubmit({ email }, { setSubmitting }) {
    const { putData } = this.props,
      data = {
        email
      }

    putData(FORGOT_PASSWORD, data)
    setSubmitting(false)
  }

  render() {
    return (
      <ForgotPasswordComponent
        {...this.props}
        formSubmit={this.formSubmit.bind(this)}
      />
    )
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      putData
    },
    dispatch
  )
}

ForgotPassword.propTypes = {
  history: PropTypes.any.isRequired,
  putData: PropTypes.func.isRequired
}

export default connect(
  null,
  mapDispatchToProps
)(ForgotPassword)
