import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import qs from 'qs'

import SetPasswordComponent from '../../../components/Account/SetPassword'

import { putData } from '../../../actions'
import { SET_PASSWORD } from '../../../constants'

class SetPassword extends Component {
  componentWillMount() {
    const { history, location } = this.props,
      token = localStorage.getItem('token'),
      search = qs.parse(location.search.slice(1))

    if (token) {
      history.replace('/')
    }
    if (!search.token) {
      history.replace('/')
    }
  }

  formSubmit({ password }, { setSubmitting }) {
    const { putData, history, location } = this.props,
      search = qs.parse(location.search.slice(1)),
      { token } = search,
      data = {
        token,
        password
      }

    putData(SET_PASSWORD, data).then(() => {
      setTimeout(() => history.replace('/'), 1000)
    })

    setSubmitting(false)
  }

  render() {
    return (
      <SetPasswordComponent
        {...this.props}
        formSubmit={this.formSubmit.bind(this)}
      />
    )
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      putData
    },
    dispatch
  )
}

SetPassword.propTypes = {
  history: PropTypes.any.isRequired,
  location: PropTypes.any.isRequired,
  putData: PropTypes.func.isRequired
}

export default connect(
  null,
  mapDispatchToProps
)(SetPassword)
