import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import ClientComponent from '../../../components/Tenancy/Clients'

import {
  toggleModal,
  loadGridItem,
  getData,
  putData,
  postData,
  requestDataSuccess
} from '../../../actions'
import { FETCH_CLIENT } from '../../../constants/action-types'
import GET_CLIENT from '../../../components/Tenancy/Clients/data.json'

class Client extends Component {
  componentDidMount() {
    //this.props.getData(GET_CLIENT, FETCH_CLIENT)
    this.props.requestDataSuccess(GET_CLIENT, FETCH_CLIENT)
  }

  actionClick(id, action, data, modalVisible) {
    if (action === 'edit') {
      this.modalToggle(action, modalVisible)
      this.props.loadGridItem(id, data)
    }
  }

  modalToggle(action, modalVisible) {
    this.props.toggleModal(action, modalVisible)
  }

  formSubmit(values, { setSubmitting }) {
    setSubmitting(false)
  }

  render() {
    return (
      <ClientComponent
        {...this.props}
        formSubmit={this.formSubmit.bind(this)}
        actionClick={this.actionClick.bind(this)}
        modalToggle={this.modalToggle.bind(this)}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    modalActionName: state.main.modal.action,
    modalVisible: state.main.modal.visible,
    selectedClient: state.main.modal.selectedItem,
    clientsData: state.clients.clientsData
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      toggleModal,
      loadGridItem,
      getData,
      putData,
      postData,
      requestDataSuccess
    },
    dispatch
  )
}

Client.propTypes = {
  toggleModal: PropTypes.func.isRequired,
  loadGridItem: PropTypes.func.isRequired,
  getData: PropTypes.func.isRequired,
  putData: PropTypes.func.isRequired,
  postData: PropTypes.func.isRequired,
  requestDataSuccess: PropTypes.func.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Client)
